# todo

- https://github.com/wessberg/cjstoesm

- Automatisera bygget, under develop
  https://www.typescripttutorial.net/typescript-tutorial/nodejs-typescript/
- Explain commonjs and esm
  https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
  https://dev.to/remshams/rolling-up-a-multi-module-system-esm-cjs-compatible-npm-library-with-typescript-and-babel-3gjg
- Build esm and cjs from typescript
  https://blog.logrocket.com/publishing-node-modules-typescript-es-modules/
- Lerna + typescript
  https://gitlab.com/Swoofty/lerna-typescript-template
