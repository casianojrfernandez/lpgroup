import errors from "@feathersjs/errors";

const { NotFound } = errors;

export class Ready {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  // eslint-disable-next-line no-unused-vars
  async find(params) {
    if (this.app.get("status").ready === "OK") return { code: 200, status: "OK" };
    throw new NotFound(`Not ready. Code: ${this.app.get("status").ready}`);
  }
}
