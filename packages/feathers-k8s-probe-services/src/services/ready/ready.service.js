import { Ready } from "./ready.class";
import hooks from "./ready.hooks";

export default (app) => {
  const options = {
    paginate: app.get("paginate"),
  };
  // Initialize our service with any options it requires
  app.use("/ready", new Ready(options, app));
  // Get our initialized service so that we can register hooks
  const service = app.service("ready");
  service.hooks(hooks);
};
