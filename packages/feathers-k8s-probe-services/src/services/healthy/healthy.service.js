import { Healthy } from "./healthy.class";
import hooks from "./healthy.hooks";

export default (app) => {
  const options = {
    paginate: app.get("paginate"),
  };
  app.use("/healthy", new Healthy(options, app));
  const service = app.service("healthy");
  service.hooks(hooks);
};
