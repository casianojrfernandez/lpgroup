import healthy from "./services/healthy/healthy.service";
import ready from "./services/ready/ready.service";

export { default as healthy } from "./services/healthy/healthy.service";
export { default as ready } from "./services/ready/ready.service";

// Configure
export default (app) => {
  app.configure(healthy);
  app.configure(ready);
};
