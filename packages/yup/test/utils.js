import * as yup from "../src";

export function validate(data, schema) {
  const valid = yup.validate(data, yup.object(schema));
  return expect(valid);
}
