import * as yup from "yup";

/**
 * defaultNull - field can be null and defaults to null
 *
 * exammple: password: cy.string().defaultNull(),
 */
// eslint-disable-next-line func-names
yup.addMethod(yup.mixed, "defaultNull", function () {
  return this.nullable().default(null);
});

export { yup };
export { mixed, lazy, boolean, number } from "yup";
export * from "./utils";
export * from "./general";
export * from "./standard";
export * from "./hooks";
export * from "./YupError";
