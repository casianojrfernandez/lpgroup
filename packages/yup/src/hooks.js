import { validate, validateKeys } from "./utils";
// TODO: Can probably remove options.schema
export function validReq(options = {}) {
  return async (context) => {
    if (context.method === "create" || context.method === "update") {
      context.data = await validate(
        context.data,
        (options.schema && options.schema.request) || context.service.options.schema.request
      );
    } else if (context.method === "patch") {
      // if external request validate field is included in pick.
      if (context.params.provider && options.pick) {
        validateKeys(context.data, options.pick);
      }
      // TODO: Check that only keys from requestSchema
      // are in the request json.
      // eslint-disable-next-line
    }
    return context;
  };
}

export function validDB() {
  return async (context) => {
    context.data = await validate(context.data, context.service.options.schema.db);
    return context;
  };
}

// TODO: depricated validateRequest, validateDatabase use validReq, validDB
export { validReq as validateRequest };
export { validDB as validateDatabase };
