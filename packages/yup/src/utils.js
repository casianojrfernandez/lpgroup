import { omit } from "lodash-es";
import * as general from "./general";
import { YupError } from "./YupError";

// Used to build request and database schemas used by the services.
// This is the two main validation schemas.
// myLock - can exist on all collections
export function buildValidationSchema(requestSchema, dbSchema) {
  return {
    request: general.object(requestSchema),
    db: general.object(requestSchema).shape(dbSchema).shape({ myLock: general.string() }),
    requestSchema,
    dbSchema,
    // TODO: Replace with  filterRequestColumns
    removeDbSchemaKeys: (mongoRecord) =>
      omit(mongoRecord, [...Object.keys(dbSchema), "myLock", "url"]),
  };
}

export function buildValidationSchemaMulti(requestSchema, dbSchema) {
  return {
    request: general.arrayObject(requestSchema),
    db: general.array(
      general.object(requestSchema).shape(dbSchema).shape({ myLock: general.string() })
    ),
    dbSchema,
    removeDbSchemaKeys: (mongoRecord) =>
      omit(mongoRecord, [...Object.keys(dbSchema), "myLock", "url"]),
  };
}

export function mergeValidationsSchema(validationSchema1, validationSchema2) {
  if (!validationSchema2) return validationSchema1;
  return buildValidationSchema(
    { ...validationSchema1.requestSchema, ...validationSchema2.requestSchema },
    { ...validationSchema1.dbSchema, ...validationSchema2.dbSchema }
  );
}

/**
 * Filter a database row with only the keys that are in the request schema.
 *
 * @param {*} row - A row from a datbase
 * @param {*} requestSchema - All keys on first level that should be kept/filtered
 */
export function filterRequestColumns(row, requestSchema) {
  return row.filter((v) => requestSchema.includes(v));
}

export function validate(data, validateSchema) {
  return validateSchema
    .validate(data, { strict: false, abortEarly: false, stripUnknown: false })
    .catch((err) => {
      throw new YupError(err);
    });
}

/**
 * Object keys in `data` must exist in array `keys`
 * Otherwise throw exception
 */
export function validateKeys(data, keys) {
  Object.keys(data).forEach((key) => {
    if (!keys.includes(key)) {
      throw new YupError({ message: `"${key}" is not allowed` });
    }
  });
}
