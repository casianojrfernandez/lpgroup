/* eslint-disable no-template-curly-in-string */
import { isObject } from "lodash-es";
import { format } from "date-fns";
import * as yup from "yup";

export function array(objSchema) {
  return yup.array(objSchema).ensure();
}

export function object(objSchema) {
  return yup.object(objSchema).default({}).noUnknown();
}

export function arrayObject(objSchema) {
  return array(object(objSchema).required()).default([]);
}

// TODO: some property to validate minmax of keys. So no empty
//       objects are allowed.
export function lazyObject(objSchema, nullable = false) {
  return yup.lazy((value) => {
    const ret = {};
    if (isObject(value)) {
      Object.keys(value).forEach((key) => {
        // TODO validate key
        if (nullable) ret[key] = object(objSchema).defaultNull();
        else ret[key] = object(objSchema);
      });
    }
    return object(ret);
  });
}

const isString = (val) => val === null || typeof val === "string" || val instanceof String;

/**
 * Check that a string really is a string or throw TypeError
 *
 * Example:
 *   yup.string().transform(stringTypeCheck).trim().default("cow"),
 *
 * Why it's needed:
 * yup.string().trim() - can't handle an array value. string() doesn't transform it to
 * a string or validate that it's not a string.
 *
 * If using yup.string().strict().trim().default("cow") - Strict will validate that
 * it's not an array and really is a string. But at the same time it will disable the
 * trim and default transformation.
 *
 * Need to be used in combination with the transform function. Because yup first transform
 * values and then validates. If instead using test() the trim() function will be executed
 * before the typecheck and throw error val.trim doesnt exist.
 */
// eslint-disable-next-line no-unused-vars
function stringTypeCheck(value, originalValue) {
  if (!isString(value))
    throw new TypeError(`Value need to be of type string, ${JSON.stringify(value)}`);
  return value;
}

export function string() {
  return yup.string().transform(stringTypeCheck).trim();
}

export function percentage() {
  return string();
}

export function minMax(min, max) {
  return yup.number().integer().min(min).max(max);
}

export function buildUrl(route, key) {
  // eslint-disable-next-line func-names
  return yup.lazy(function () {
    const url = this.resolve(key);
    return yup.string().default(url);
  });
}

export function timestamp() {
  return yup
    .number()
    .integer()
    .test("is-timestamp", "${path} is not a timestamp.", (value) => {
      if (value === undefined || value === null) return true;
      return new Date(value).getTime() > 0;
    });
}

export function isValidDate(d) {
  return d instanceof Date && !Number.isNaN(d);
}

export function fromDateTime() {
  return string().test(
    "is-fromdatetime",
    "${path} is not a date with format format YYYY-MM-DDTHH:mm:ss.sss[Z].",
    (value) => {
      if (value === undefined || value === null) return true;
      const d = new Date(value);
      return isValidDate(d) && d.toISOString() === value;
    }
  );
}

export function fromDate() {
  return string().test(
    "is-fromdate",
    "${path} is not a date with format format yyyy-MM-dd.",
    (value) => format(new Date(value), "yyyy-MM-dd") === value
  );
}

export function fromTime() {
  return string().test(
    "is-fromtime",
    "${path} is not a time with format format HH:mm.",
    (value) => format(new Date(`2000-01-01 ${value}`), "HH:mm") === value
  );
}

export function toDateTime(fromKey) {
  return fromDateTime().test(
    "is-todatetime",
    "To date ${path} should be later than from date.",
    function test(value) {
      if (value === undefined || value === null) return true;
      const from = new Date(this.parent[fromKey]);
      const to = new Date(value);
      return to.getTime() >= from.getTime();
    }
  );
}

export function toDate(fromKey) {
  return fromDate().test(
    "is-todate",
    "To date ${path} should be later than from date.",
    function test(value) {
      const from = new Date(this.parent[fromKey]);
      const to = new Date(value);
      return to.getTime() >= from.getTime();
    }
  );
}

export function toTime(fromKey) {
  return fromTime().test(
    "is-totime",
    "To time ${path} should be later than from time.",
    function test(value) {
      const from = new Date(`2000-01-01 ${this.parent[fromKey]}`);
      const to = new Date(`2000-01-01 ${value}`);
      return to.getTime() >= from.getTime();
    }
  );
}
