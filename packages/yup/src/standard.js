import { nanoid as nano } from "nanoid";
import { v4 as uuidv4 } from "uuid";
import * as yup from "yup";

import { arrayObject, fromDateTime, object, string, timestamp, toDateTime } from "./general";

const LABEL_TEXT_MIN_SIZE = 1;
const LABEL_TEXT_MAX_SIZE = 85;
const SHORT_TEXT_MAX_SIZE = 250;
const MEDIUM_TEXT_MAX_SIZE = 600;
const LONG_TEXT_MAX_SIZE = 50000;
const URL_MAX_SIZE = 255;
const IMAGE_ALT_MAX_SIZE = 125;
// TODO: rename id, nanoid and uuid maybe autoUuid
export function id() {
  return yup.lazy(() => yup.string().default(uuidv4()));
}

export function nanoid() {
  return yup.lazy(() => yup.string().default(nano()));
}

export function uuid() {
  return string();
}

export function alias() {
  return string().min(LABEL_TEXT_MIN_SIZE).max(LABEL_TEXT_MAX_SIZE).lowercase();
}

export function title() {
  return string().min(LABEL_TEXT_MIN_SIZE).max(LABEL_TEXT_MAX_SIZE);
}

export function labelText() {
  return string().max(LABEL_TEXT_MAX_SIZE);
}

export function shortText() {
  return string().max(SHORT_TEXT_MAX_SIZE);
}

export function mediumText() {
  return string().max(MEDIUM_TEXT_MAX_SIZE);
}

export function longText() {
  return string().max(LONG_TEXT_MAX_SIZE);
}

export function description() {
  return object({
    short: shortText().defaultNull(),
    text: longText().defaultNull(),
  });
}

export function imageUri() {
  return yup.string();
}

export function pdfUri() {
  return yup
    .string()
    .nullable()
    .matches(/^data:application\/pdf;base64/);
}

export function referenceNumber() {
  return yup.number().integer().min(0);
}

// This can hold both positive and negative numbers.
export function amount() {
  return yup.number().integer();
}

export function positiveAmount() {
  return amount().min(0);
}

export function currency() {
  return yup.string().oneOf(["sek"]);
}

export function vatId() {
  return yup.string().oneOf(["vat-06", "vat-12", "vat-25"]);
}

export function bankGiro() {
  return labelText().defaultNull();
}

export function postGiro() {
  return labelText().defaultNull();
}

export function email() {
  // todo: email should handle
  //    cy.email().defaultNull();
  //    cy.email().required();
  return string().lowercase().max(254); // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address/44317754
}

// TODO
// eslint-disable-next-line max-len
// const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
// yup.string().matches(phoneRegExp, "Ange ett giltigt telefonnummer"),
export function phone() {
  return string().max(LABEL_TEXT_MAX_SIZE).defaultNull();
}

export function tags() {
  return string().min(0).max(MEDIUM_TEXT_MAX_SIZE).lowercase();
}

export function url() {
  return string().max(URL_MAX_SIZE);
}

export function images() {
  return arrayObject({
    _id: id(),
    src: url().required(),
    alt: string().max(IMAGE_ALT_MAX_SIZE).required(),
  });
}

export function notRequiredImage() {
  return object({
    _id: id(),
    src: url().defaultNull(),
    alt: string().max(IMAGE_ALT_MAX_SIZE).defaultNull(),
  });
}

export function socialSecurityNumber() {
  return labelText().defaultNull();
}

export function organisationNumber() {
  return labelText().defaultNull();
}

export function address() {
  return object({
    street: labelText().defaultNull(),
    city: labelText().defaultNull(),
    state: labelText().defaultNull(),
    zipCode: labelText().defaultNull(),
    country: labelText().defaultNull(),
  });
}

export function emailContact() {
  return object({
    lastName: labelText().defaultNull(),
    firstName: labelText().defaultNull(),
    email: email().defaultNull(),
    phone: phone().defaultNull(),
  });
}

export function location() {
  return object({
    latitude: yup.number().min(-80).max(80).defaultNull(),
    longitude: yup.number().min(-180).max(180).defaultNull(),
  });
}

export function publish() {
  return object({
    active: yup.boolean().default(false),
    from: fromDateTime().defaultNull(),
    to: toDateTime("from").defaultNull(),
  });
}

export function changed() {
  return object({
    by: uuid().required(),
    at: timestamp().required(),
  });
}

export function owner() {
  return object({
    user: object({
      _id: uuid().required(),
    }).required(),
    organisation: object({
      alias: alias().required(),
    }).required(),
  });
}

export function userOwner() {
  return object({
    user: object({
      _id: uuid().required(),
    }).required(),
  });
}

export function meta() {
  return yup.lazy((value) => {
    switch (typeof value) {
      case "number":
        return yup.number();
      case "string":
        return yup.string();
      case "object":
        return yup.object();
      // This set the default value to {}
      case "undefined":
        return yup.object().default({});
      default:
        return yup.mixed();
    }
  });
}
