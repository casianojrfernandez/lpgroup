/* eslint-disable no-console */
/* eslint-disable no-use-before-define */
import debugRoot from "debug";
import amqp from "amqplib";
import { sleep } from "@lpgroup/feathers-utils";

const debug = debugRoot("rabbitmq");

// TODO:
// - Change to this wrapper, https://github.com/guidesmiths/rascal ??
// - Error handling, when loosing connection https://github.com/squaremo/amqp.node/issues/266

/**
 * Start a rabbitmq server used by microservices
 *
 * options {
 *   "uri": "amqps://user:password@wolf.rmq.cloudamqp.com/xxx",
 *   "queue": "dev"
 * }
 */
export async function init(app, options) {
  return initWithOptions(options);
}

export async function initWithOptions(options) {
  return setupRabbitMQ(options);
}

export async function setupRabbitMQ(options) {
  const { conn, channel } = await getChannel(options);

  return {
    conn,
    channel,

    consume(queue, consumer) {
      console.log(`RabbitMQ Consume queue ${queue}`);
      channel
        .assertQueue(queue, { durable: true })
        .then(() => channel.prefetch(1))
        .then(
          () =>
            channel.consume(queue, (msg) => {
              const body = msg.content.toString();
              debug(`  Incomming on ${queue} ${body}`);
              consumer
                .consume(body)
                .then(() => {
                  channel.ack(msg);
                })
                .catch(async (e) => {
                  console.log(e);
                  // Return to queue after 5 seconds and try again.
                  await sleep(5000);
                  channel.nack(msg);
                });
            }),
          { noAck: false, requeue: true }
        );
    },

    async closePlugin() {
      await channel.close();
      await conn.close();
    },

    async sentToQueue(queue, request) {
      return channel
        .assertQueue(queue, { durable: true })
        .then(() => {
          channel.sendToQueue(queue, Buffer.from(request), { persistent: true });
          debug("Send to queue %s", queue);
        })
        .catch((err) => {
          console.warn(`RabbitMq: ${err}`);
        });
    },
  };
}

async function getChannel(options) {
  let conn;
  let channel;
  const uri = `${options.uri}?heartbeat=30`;
  await amqp
    .connect(uri)
    .then(async (v) => {
      conn = v;
      console.log(`Connected to RabbitMQ server: ${options.uri} with queue ${options.queue}`);
      process.once("SIGINT", () => conn.close());
      channel = await conn.createChannel();
      return channel;
    })
    .catch((err) => {
      console.error(`Can't connect to RabbitMQ ${uri}\n`, err.message);
      throw (`Can't connect to RabbitMQ ${uri}\n`, err.message);
    });

  channel.once("error", (err) => {
    console.error(`Can't connect to RabbitMQ ${uri}\n`, err.message);
    if (err.code !== 404) {
      throw err;
    }
  });

  return { conn, channel };
}
