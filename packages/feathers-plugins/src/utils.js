const pluginByName = {};
const plugins = [];

// TODO: deprecate
export function addPlugin(app, name, plugin, options = {}) {
  pluginByName[name] = plugin.init(app, options);
  plugins.push(pluginByName[name]);
}

export function addPluginWithOptions(name, plugin, options = {}) {
  pluginByName[name] = plugin.initWithOptions(options).then(async (v) => {
    if (options.readyServer) {
      await v.waitOnServerReady(options.readyServer);
    }
    return v;
  });

  plugins.push(pluginByName[name]);
}

export function closePlugins() {
  Object.values(pluginByName).forEach(async (plugin) => {
    const p = await plugin;
    // It's not required to have a closePlugin function in a plugin
    if (p.closePlugin) p.closePlugin();
  });
}

export function hasPlugin(name) {
  return name in pluginByName;
}

export function checkHasPlugin(name) {
  if (!hasPlugin(name)) throw Error(`Plugin ${name} are not added`);
}

export async function onPluginReady(name) {
  checkHasPlugin(name);
  return pluginByName[name];
}

export async function onPluginsReady() {
  return Promise.all(plugins);
}
