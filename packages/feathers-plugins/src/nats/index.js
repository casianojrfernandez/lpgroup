/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
import nats from "nats";
import debugRoot from "debug";
import { checkExpected } from "@lpgroup/feathers-utils";

const debug = debugRoot("nats");

export { natsYupError } from "./natsYupError";

/**
 * Start a nats server used by microservices
 *
 * "nats": {
 *   "name": "lpgroup-ng",
 *   "uri": "nats://localhost:4222",
 *   "queue": "file-ng-dev",
 *   "messagePrefix": "dev",
 *   "versions": {
 *     "post.document.transaction-forms.request": "v1",
 *     "post.document.transaction-forms.response": "v1",
 *   }
 * },
 */

export async function createNatsServer(options) {
  const { queue } = options;
  const nc = nats.connect({
    name: `${options.name} ${options.messagePrefix}`,
    uri: options.uri,
    reconnect: true,
    maxReconnectAttempts: -1,
    reconnectTimeWait: 2000,
    // TODO: ,json: true
  });

  nc.on("connect", () => {
    // Do something with the connection
    console.log(`Connected to NATS server: ${options.uri} and queue ${options.queue}`);
  });

  nc.on("disconnect", () => {
    console.log(`disconnect ${options.uri} ${options.queue}`);
  });

  nc.on("reconnecting", () => {
    console.log(`reconnecting ${options.uri} ${options.queue}`);
  });

  nc.on("reconnect", () => {
    console.log(`reconnect ${options.uri} ${options.queue}`);
  });

  nc.on("close", () => {
    console.log(`close ${options.uri} ${options.queue}`);
  });

  nc.on("error", (err) => {
    console.error("error", err, options);
  });

  nc.closePlugin = () => {
    nc.close();
  };

  // TODO:
  // eslint-disable-next-line no-unused-vars
  nc.waitOnServerReady = async (url) => {};

  const validate = async (request, service) => {
    if (service.options.schema && service.options.schema.request)
      return validateYup(request, service.options.schema.request);
    return request;
  };
  nc.sub = (subject, service) => {
    const subscribeSubject = getSubscribeSubject(options, subject);
    console.log(`NATS Subscribe on ${subscribeSubject}`);

    nc.subscribe(subscribeSubject, { queue }, (msg, reply) => {
      try {
        const request = JSON.parse(msg);
        debug(`Incoming on "${subscribeSubject}" ${reply || ""}`);
        validate(request, service)
          .then((v) => {
            service.request(v, reply).catch((err) => {
              service.error(request, err, reply);
            });
          })
          .catch((err) => {
            service.error(request, err, reply);
          });
      } catch (err) {
        debug(`Incoming error on "${subscribeSubject}" ${reply || ""} ${err.message}`);
        service.error({}, err, reply);
      }
    });
  };

  nc.pub = async (subject, msg, optionsOverride = {}) => {
    const subscribeSubject = getSubscribeSubject(options, subject, optionsOverride);
    debug(`Publish on "${subscribeSubject}"`);
    const request = JSON.stringify(msg);
    return nc.publish(subscribeSubject, request);
  };

  // Store subscribtion promises from subscribe()
  // incomming() can then wait for them to finish.
  nc.subscribes = {};

  nc.subExpect = (subject, configOptions = {}) => {
    const subscribeSubject = getSubscribeSubject(options, subject);
    const promise = new Promise((resolve, reject) => {
      nc.subscribe(subscribeSubject, (msg, reply, _subject, sid) => {
        debug(`Incoming on "${subscribeSubject}" ${reply || ""}`);
        const request = JSON.parse(msg);
        if (
          checkExpected(request, { ...configOptions, ignoreKeyCompare: options.ignoreKeyCompare })
        ) {
          reject(sid);
        } else {
          resolve(sid);
        }
      });
    });

    nc.subscribes[subscribeSubject] = nc.subscribes[subscribeSubject]
      ? nc.subscribes[subscribeSubject].push(promise)
      : [promise];
  };

  nc.waitForSubExpect = async (subject) => {
    const subscribeSubject = getSubscribeSubject(options, subject);
    debug(`Wait for incoming: ${subscribeSubject}`);
    if (nc.subscribes[subscribeSubject] && nc.subscribes[subscribeSubject].length > 0) {
      return Promise.all(nc.subscribes[subscribeSubject])
        .then(async (result) => {
          await Promise.all(
            result.map(async (sid) => {
              debug(`Unsubscribe ${subscribeSubject} ${sid}`);
              await nc.unsubscribe(sid);
              delete nc.subscribes[subscribeSubject];
            })
          );
        })
        .catch(() => {
          debug(`Failed incoming: ${subscribeSubject}`);
        });
    }
    debug(`No subscription added for: ${subscribeSubject}`);
    return undefined;
  };

  return nc;
}

/**
 * Adding int.v2 in the subject "int.v2.post.votes.cats.request"
 */
function getSubscribeSubject(configOptions, subject, optionsOverride = {}) {
  const prefix = optionsOverride.messagePrefix || configOptions.messagePrefix || "NA";
  const version =
    optionsOverride.versions || (configOptions.versions && configOptions.versions[subject]);
  if (version) return `${prefix}.${version}.${subject}`;
  return `${prefix}.${subject}`;
}

function validateYup(data, validateSchema) {
  return validateSchema.validate(data, { strict: false, abortEarly: false, stripUnknown: false });
}

export async function initWithOptions(options) {
  return createNatsServer(options);
}

export async function init(app) {
  return initWithOptions(app.get("nats"));
}
