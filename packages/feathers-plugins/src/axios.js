import { setupAxios, instance } from "@lpgroup/feathers-utils";

export async function initWithOptions(options) {
  setupAxios(options);
  return instance();
}

export async function init(app, options) {
  return initWithOptions(options);
}
