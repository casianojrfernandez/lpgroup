/* eslint-disable no-console */
/* eslint-disable no-use-before-define */
import { hooks } from "@lpgroup/feathers-mongodb-hooks";
import mongodb from "mongodb";

// System globals to access MongoDb
let client = null;
let database = null;

export async function init(app) {
  const uri = app.get("mongodb");
  return initWithOptions({ uri });
}

export async function initWithOptions(options) {
  if (!client) {
    await createMongoClient(options.uri);
  }
  return { client, database, closePlugin };
}

async function createMongoClient(uri) {
  const databaseName = uri.substr(uri.lastIndexOf("/") + 1);
  client = new mongodb.MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  database = await client
    .connect()
    .then((c) => {
      console.log(`Connected to Mongo db: ${databaseName}`);
      const db = c.db(databaseName);
      // TODO Remove
      db.createIndexThrowError = createIndexThrowError;
      return db;
    })
    .catch((error) => {
      console.log("Mongo error: ", error);
    });
  return database;
}

async function closePlugin() {
  this.db.close();
  this.client.close();
}

/**
 * Regular mongodb createIndex with error catcher.
 */
async function createIndexThrowError(table, fieldOrSpec, options) {
  return this.collection(table)
    .createIndex(fieldOrSpec, options)
    .then((index) => {
      console.log(`Created index: ${table}.${index}`);
    })
    .catch((err) => {
      console.error("error", err.message);
      throw Error(`error${JSON.stringify(err.message, null, 2)}`);
    });
}

export function startSession() {
  return async (context) => {
    const startTrx = hooks.startSession({ client, database: await database });
    return startTrx(context);
  };
}

const { endSession, errorSession } = hooks;
export { endSession, errorSession };
