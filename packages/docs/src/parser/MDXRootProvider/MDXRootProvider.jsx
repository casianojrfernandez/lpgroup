import React from "react";
import { MDXProvider } from "@mdx-js/react";
import PropTypes from "prop-types";
import { mdxComponents } from "../../components";

export const MDXRootProvider = ({ children, customRender = mdxComponents }) => {
  return <MDXProvider components={customRender}>{children}</MDXProvider>;
};

MDXRootProvider.propTypes = {
  children: PropTypes.any.isRequired,
  customRender: PropTypes.object,
};
