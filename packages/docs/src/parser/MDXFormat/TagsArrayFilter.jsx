import React from "react";
import PropTypes from "prop-types";

/**
 * Generate array with only `type` react elements
 */
function filterCodeBlock(children, type) {
  return React.Children.map(children, (child) => {
    if (type.includes("all")) return child;
    return child && child.props && type.includes(child.props.mdxType) ? child : undefined;
  });
}

/**
 * Parse each mdx file for tagElement
 */
function filterCodeBlocks(children, props) {
  const code = filterCodeBlock(children, props.elementTags);
  return <div>{props.elementTags === "all" ? children : code}</div>;
}

/**
 * Loops each mdx file
 */
function loopEachMDX(props) {
  // props.children == <pages>
  const childrenOfPages = props.children.type({}).props.children;
  return React.Children.map(childrenOfPages, (child) => {
    const content = child.type({}).props.children;
    return filterCodeBlocks(content, props);
  });
}

export function TagsArrayFilter(props) {
  return <div>{loopEachMDX(props)}</div>;
}

TagsArrayFilter.propTypes = {
  children: PropTypes.any.isRequired,
  elementTags: PropTypes.array,
};
