import React from "react";
import { MDXProvider } from "@mdx-js/react";
import { CssBaseline } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { Theme, mdxComponents } from "../../components";
import { TagsArrayFilter } from "./TagsArrayFilter";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    [theme.breakpoints.down("xs")]: theme.mixins.toolbar,
  },
  content: {
    padding: theme.spacing(6, 3),
  },
}));

export function MDXFormat({ children, customRender = mdxComponents, elementTags = ["all"] }) {
  const classes = useStyles();
  return (
    <Theme>
      <MDXProvider components={customRender}>
        <CssBaseline />
        <div className={classes.toolbar} />
        <div className={classes.content}>
          <TagsArrayFilter elementTags={elementTags}>{children}</TagsArrayFilter>
        </div>
      </MDXProvider>
    </Theme>
  );
}

MDXFormat.propTypes = {
  children: PropTypes.any.isRequired,
  customRender: PropTypes.object,
  elementTags: PropTypes.array,
};
