import React from "react";
import Highlight, { defaultProps } from "prism-react-renderer";
import theme from "prism-react-renderer/themes/duotoneLight";
import { Example } from "../mdx";

theme.plain.backgroundColor = "rgb(250, 250, 250)";

export function CodeBlock({ children, ...props }) {
  // Get the title from a tag like this. MDXJS splits on space
  // ```json title=Response example
  const t = props.metastring.split("=");
  const method = t.length > 1 ? t[0] : "";
  const title = t.length > 1 ? t[1] : "";

  const language = props.className.replace(/language-/, "");

  return (
    <Example method={method} title={title}>
      <Highlight {...defaultProps} theme={theme} code={children.trim()} language={language}>
        {({ className, style, tokens, getLineProps, getTokenProps }) => (
          <pre className={className} style={{ ...style, margin: "0px", fontSize: "14px" }}>
            {tokens.map((line, i) => (
              <div key={i} {...getLineProps({ line, key: i })}>
                {line.map((token, key) => (
                  <span key={key} {...getTokenProps({ token, key })} />
                ))}
              </div>
            ))}
          </pre>
        )}
      </Highlight>
    </Example>
  );
}
