import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Divider, IconButton, Toolbar } from "@material-ui/core/";
import MenuIcon from "@material-ui/icons/Menu";
import { useInfo } from "./InfoProvider";

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: theme.spacing(1),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
}));

export function AppToolBar(props) {
  const { logo: Logo, logoClassName } = useInfo();
  const classes = useStyles();
  const theme = useTheme();
  return (
    <>
      <Toolbar style={{ minHeight: theme.spacing(8), paddingLeft: theme.spacing(2) }}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={props.handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        <div style={logoClassName}>
          <Logo />
        </div>
      </Toolbar>
      <Divider />
    </>
  );
}
