import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "row",
  },
  headerContainer: (props) => ({
    padding: theme.spacing(0, 2),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(0, 4),
    },
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(0, 4),
    },
    [theme.breakpoints.up("lg")]: {
      padding: theme.spacing(0, 8),
    },
  }),
  leftContainer: (props) => ({
    flex: 2,
    minWidth: 0,
    padding: theme.spacing(0, 2),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(0, 4),
    },
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(0, 4),
    },
    [theme.breakpoints.up("lg")]: {
      padding: theme.spacing(0, 8),
    },
  }),
  rightContainer: (props) => ({
    flex: 1,
    minWidth: 0,
    overflow: "auto",
    wordBreak: "break-all",
    padding: theme.spacing(0, 2),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(0, 4),
    },
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(0, 4),
    },
    [theme.breakpoints.up("lg")]: {
      padding: theme.spacing(0, 8),
    },
  }),
  divider: {
    height: 1,
    margin: theme.spacing(8, 0),
  },
}));

export function Section(props) {
  const classes = useStyles();
  if (props.leftColumn.length === 0 && props.rightColumn.length === 0) {
    return <></>;
  }

  return (
    <>
      <Grid container className={classes.container}>
        <Grid item xs={12} md={12}>
          <Box className={classes.headerContainer}>{props.header}</Box>
        </Grid>
        <Grid item xs={12} md={6}>
          <Box className={classes.leftContainer}>{props.leftColumn}</Box>
        </Grid>
        <Grid item xs={12} md={6} className={classes.rightContainer} zeroMinWidth>
          {props.rightColumn}
        </Grid>
        <Grid item xs={12} md={12}>
          <Divider flexItem className={classes.divider} />
        </Grid>
      </Grid>
    </>
  );
}
