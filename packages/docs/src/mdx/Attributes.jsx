import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  title: {
    paddingRight: theme.spacing(1),
    fontWeight: "bold",
    marginBottom: 0,
  },
  type: {
    color: "#2a2f45",
    marginBottom: 0,
  },
  desc: {
    marginTop: 0,
    marginBottom: 0,
  },
  divider: {
    height: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  method: {
    margin: 1,
  },
}));

export function Attributes(props) {
  return <>{props.children}</>;
}

export function Attribute(props) {
  const classes = useStyles();
  return (
    <>
      <Divider flexItem className={classes.divider} />
      <Box>
        <Box display="flex" className={classes.method}>
          <Typography pr={2} className={classes.title}>
            {props.title}
          </Typography>
          <Typography className={classes.type}>{props.type}</Typography>
        </Box>
        <Typography className={classes.desc}>{props.children}</Typography>
      </Box>
    </>
  );
}
