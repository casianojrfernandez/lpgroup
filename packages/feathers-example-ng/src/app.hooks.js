import { startSession, endSession, errorSession } from "@lpgroup/feathers-plugins/mongodb";
import { requestLog, setQuery } from "@lpgroup/feathers-utils/hooks";
import feathersHooksCommon from "feathers-hooks-common";

const { discard, iff, isProvider } = feathersHooksCommon;

export default {
  before: {
    all: [requestLog(), setQuery()],
    find: [],
    get: [],
    create: [startSession()],
    update: [startSession()],
    patch: [startSession()],
    remove: [startSession()],
  },

  after: {
    all: [
      requestLog(),
      iff(isProvider("external"), discard("organisationAlias", "myLock", "password")),
    ],
    find: [],
    get: [],
    create: [endSession()],
    update: [endSession()],
    patch: [endSession()],
    remove: [endSession()],
  },

  error: {
    all: [requestLog()],
    find: [],
    get: [],
    create: [errorSession()],
    update: [errorSession()],
    patch: [errorSession()],
    remove: [errorSession()],
  },
};
