import { addPlugin, mongodb, nats, sync } from "@lpgroup/feathers-plugins";

export default (app) => {
  // TODO: use addPluginWithOptions
  addPlugin(app, "mongodb", mongodb);
  addPlugin(app, "nats", nats);
  addPlugin(app, "sync", sync);
};
