import compress from "compression";
import helmet from "helmet";
import cors from "cors";

import feathers from "@feathersjs/feathers";
import configuration from "@feathersjs/configuration";
import express from "@feathersjs/express";
import socketio from "@feathersjs/socketio";

import { errorHandler } from "@lpgroup/feathers-utils";

import plugins from "./plugins/index";
import middleware from "./middleware/index";
import services from "./services/index";
import appHooks from "./app.hooks";
import channels from "./channels";

const app = express(feathers());

// Load app configuration
app.configure(configuration());

// Enable security, CORS, compression and body parsing
app.use(helmet({ contentSecurityPolicy: false }));
app.use(cors());
app.use(compress());
app.use(express.json({ limit: "10mb" }));
app.use(express.urlencoded({ limit: "10mb", extended: true }));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(
  socketio({ path: "/ws/" }, (io) => {
    middleware(app, io);
  })
);

app.configure(plugins);
app.configure(middleware);
app.configure(services);
app.configure(channels);

// Configure a middleware for 404s and the error handler
// Always return rest message
app.use(express.notFound());
app.use(errorHandler());
app.hooks(appHooks);

process.on("SIGTERM", () => {
  console.error("SIGTERM signal received.");
});

// Catches all unhandles promise exceptions
process.on("unhandledRejection", (reason, p) => {
  console.error("Unhandled Rejection at: Promise ", p, " reason: ", reason);
});

export default app;
