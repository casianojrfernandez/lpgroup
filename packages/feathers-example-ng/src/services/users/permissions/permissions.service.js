import { UsersPermissions } from "./permissions.class";
import hooks from "./permissions.hooks";

export default (app) => {
  const options = {
    id: "_id",
  };

  app.use("/users/:userId/permissions", new UsersPermissions(options, app));
  const service = app.service("users/:userId/permissions");
  service.hooks(hooks);
};
