import { hooks } from "@lpgroup/feathers-auth-service/hooks";

const { getPermissions } = hooks.checkPermissions;

export class UsersPermissions {
  constructor(options, app) {
    this.app = app;
  }

  async find(params) {
    return getPermissions({ app: this.app, params });
  }
}
