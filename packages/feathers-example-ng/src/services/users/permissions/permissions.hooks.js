import { auth } from "@lpgroup/feathers-auth-service/hooks";
import feathersHooksCommon from "feathers-hooks-common";

const { disallow } = feathersHooksCommon;

export default {
  before: {
    all: [auth()],
    find: [],
    get: [disallow()],
    create: [disallow()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
