import {
  authentication,
  privileges,
  usersPermissions,
  organisationsGrants,
} from "@lpgroup/feathers-auth-service";

// k8s probes
import { healthy, ready } from "@lpgroup/feathers-k8s-probe-services";

// Admin
import adminTruncateDatabase from "./admin/truncate/database/database.service";
import admin from "./admin/admin.service";

// Onboarding
import onboarding from "./onboarding/onboarding.service";
import onboardingRegisterUser from "./onboarding/register-user/register-user.service";

// Users
import users from "./users/users/users.service";

// Organisations
import organisations from "./organisations/organisations/organisations.service";

// Root
import root from "./root/root.service";

// eslint-disable-next-line no-unused-vars
export default (app) => {
  // root-endpoints (Admin, Root etc.) needs to be configured after subendpoints (status).
  // This to let app.use override GET pathes.

  // Authorization
  app.configure(authentication);
  app.configure(privileges);

  // k8s probes
  app.configure(ready);
  app.configure(healthy);

  // Admin
  app.configure(adminTruncateDatabase);
  app.configure(admin);

  // Onboarding
  app.configure(onboarding);
  app.configure(onboardingRegisterUser);

  // Users
  app.configure(users);
  app.configure(usersPermissions);

  // Organisation

  app.configure(organisationsGrants);
  app.configure(organisations);

  // Root
  app.configure(root);
};
