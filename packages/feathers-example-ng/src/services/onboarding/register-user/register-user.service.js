import { OnboardingRegisterUser } from "./register-user.class";
import hooks from "./register-user.hooks";
import schema from "./register-user.yup";

export default (app) => {
  const options = { schema };
  app.use("/onboarding/register-user", new OnboardingRegisterUser(options, app));
  const service = app.service("/onboarding/register-user");
  service.hooks(hooks);
};
