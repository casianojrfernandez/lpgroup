import * as cy from "../../../common/yup";

const requestSchema = {
  _id: cy.id(),
  email: cy.email().required(),
  password: cy.string().required(),
  firstName: cy.string().required(),
  lastName: cy.string().required(),
};

const dbSchema = {};

export default cy.buildValidationSchema(requestSchema, dbSchema);
