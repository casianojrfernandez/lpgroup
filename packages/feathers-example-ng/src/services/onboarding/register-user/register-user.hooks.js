import { validReq } from "@lpgroup/yup";
import feathersHooksCommon from "feathers-hooks-common";

const { disallow, discard, iff, isProvider } = feathersHooksCommon;

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [validReq()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [
      iff(
        isProvider("external"),
        discard(
          "privileges",
          "owner",
          "added",
          "changed",
          "password",
          "signInEnabled",
          "signInCode"
        )
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
