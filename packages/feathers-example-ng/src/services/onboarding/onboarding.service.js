import { Onboarding } from "./onboarding.class";
import hooks from "./onboarding.hooks";

export default (app) => {
  const options = {};
  app.use("/onboarding", new Onboarding(options, app));
  const service = app.service("onboarding");
  service.hooks(hooks);
};
