export class Onboarding {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find() {
    const host = this.app.get("apiDomain");

    return {
      "register-user_url": `${host}/onboarding/register-user`,
    };
  }
}
