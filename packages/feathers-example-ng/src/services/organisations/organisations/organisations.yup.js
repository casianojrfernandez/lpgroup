import * as cy from "../../../common/yup";

export const requestSchema = {
  title: cy.string().required(),
  companyName: cy.labelText().defaultNull(),
  description: cy.description(),
  visitAddress: cy.address(),
  location: cy.location(),
  theme: cy.object().shape({
    color: cy.object().shape({
      appBarBackground: cy.string(),
      appBarForeground: cy.string(),
      primary: cy.string().default("#666666"),
      secondary: cy.string().default("#cccccc"),
    }),
    logoUrl: cy.string(),
    loginImageUrl: cy.string(),
  }),
  images: cy.images(),
};

const dbSchema = {};

export default cy.buildValidationSchema(requestSchema, dbSchema);
