/* eslint-disable no-unused-vars */
export class Admin {
  constructor(options) {
    this.options = options || {};
  }

  async find(params) {
    return {
      truncate_database_url: "/admin/truncate/database",
    };
  }
}
