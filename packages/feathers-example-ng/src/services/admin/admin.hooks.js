import { auth } from "@lpgroup/feathers-auth-service/hooks";
import { disallowEnvironment } from "@lpgroup/feathers-utils/hooks";
import feathersHooksCommon from "feathers-hooks-common";

const { disallow } = feathersHooksCommon;

export default {
  before: {
    all: [auth(), disallowEnvironment({ NODE_CONFIG_ENV: "prod" })],
    find: [],
    get: [disallow()],
    create: [disallow()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
