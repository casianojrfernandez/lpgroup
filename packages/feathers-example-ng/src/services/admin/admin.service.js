import { Admin } from "./admin.class";
import hooks from "./admin.hooks";

export default (app) => {
  const options = {};
  app.use("/admin", new Admin(options, app));
  const service = app.service("admin");
  service.hooks(hooks);
};
