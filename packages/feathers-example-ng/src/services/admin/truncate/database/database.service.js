import { TruncateDatabase } from "./database.class";
import hooks from "./database.hooks";

export default (app) => {
  const options = {};
  app.use("/admin/truncate/database", new TruncateDatabase(options, app));
  const service = app.service("admin/truncate/database");
  service.hooks(hooks);
};
