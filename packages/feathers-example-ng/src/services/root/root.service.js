import { Root } from "./root.class";
import hooks from "./root.hooks";

export default (app) => {
  const options = {};
  app.use("/", new Root(options, app));
  const service = app.service("");
  service.hooks(hooks);
};
