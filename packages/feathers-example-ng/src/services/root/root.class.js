/* eslint-disable no-unused-vars */
export class Root {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find(params) {
    const host = this.app.get("apiDomain");

    return {
      authentication_url: `${host}/authentication`,
      countries_url: `${host}/countries`,
      organisation_url: `${host}/organisations`,
      users_url: `${host}/users`,
      vats_url: `${host}/vats`,
    };
  }
}
