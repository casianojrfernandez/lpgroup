/* eslint-disable no-console */
import Table from "table-layout";
import { onPluginsReady } from "@lpgroup/feathers-plugins";

import app from "./app";
import load from "./load";

const port = app.get("port");

console.log("feathers-example Engine (%s %s)", app.get("application"), app.get("version"));
const table = new Table(
  [
    { name: "env:", value: app.get("env") },
    { name: "environment: ", value: app.get("environment") },
    { name: "node_env: ", value: process.env.NODE_ENV || "NA" },
    { name: "node_config_env: ", value: process.env.NODE_CONFIG_ENV || "NA" },
    { name: "debug: ", value: process.env.DEBUG || "NA" },
  ],
  { maxWidth: 60 }
);

console.log(table.toString());

process.on("unhandledRejection", (reason, p) =>
  console.error("Unhandled Rejection at: Promise ", p, reason)
);

onPluginsReady().then(() => {
  const server = app.listen(port);
  server.on("listening", async () => {
    // Load default data into services (mongodb)
    await load(app);
    console.log("Listening on: http://%s:%d", app.get("host"), port);
    app.set("status").ready = "OK";
  });
});
