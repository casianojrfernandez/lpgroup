/* eslint-disable no-console */

// TODO: What happens when several instances starts up? Will they overwrite each other?
//       Is using PUT/Update enough to avoid missing user in database?

const allFG = {
  query: [{ key: "_id", param: "*" }],
  methods: ["find", "get"],
};

const userC = {
  query: [{ key: "owner.user._id", param: "userId" }],
  methods: ["create"],
};

const userGUP = {
  query: [{ key: "owner.user._id", param: "userId" }],
  methods: ["get", "update", "patch"],
};

const userGFUP = {
  query: [{ key: "owner.user._id", param: "userId" }],
  methods: ["get", "find", "update", "patch"],
};

const orgGUP = {
  query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
  methods: ["get", "update", "patch"],
};

const orgGFCUPR = {
  query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
  methods: ["get", "find", "create", "update", "patch", "remove"],
};

/**
 * Used by users not authenticated with /authentication
 */
function getPublicPrivilege() {
  return {
    alias: "public",
    name: "Public",
    permissions: {
      vats: allFG,
      countries: allFG,
      organisations: allFG,
    },
  };
}

/**
 * Used by users of type: device, who authenticate with deviceId
 */
function getDeviceUser() {
  return {
    alias: "device_user",
    name: "Device user",
    permissions: {},
  };
}

/**
 * Used by users of type: user, who have email/password  /authentication
 */
function getStandardUser() {
  return {
    alias: "standard_user",
    name: "Standard user",
    permissions: {
      ...getDeviceUser().permissions,
      "users": userGUP,
      "users/:userId/permissions": userGFUP,
      "organisations": userC,
    },
  };
}

function getStandardOrganisations() {
  return {
    alias: "standard_organisation",
    name: "Standard organisation",
    permissions: {
      "organisations": orgGUP,
      "organisations/:organisationAlias/grants": orgGFCUPR,
    },
  };
}

function getSuperUser() {
  const allPermissions = {
    query: [{ key: "_id", param: "*" }],
    methods: ["*"],
  };
  return {
    alias: "super_user",
    name: "Super user",
    permissions: {
      "admin": allPermissions,
      "admin/truncate/database": allPermissions,
      "organisations": allPermissions,
      "organisations/:organisationAlias/grants": allPermissions,
      "privileges": allPermissions,
      "users": allPermissions,
    },
  };
}

async function createPrivileges(app) {
  const service = app.service("privileges");
  const params = { superUser: true };
  await service.remove("public", params).catch(() => {});
  await service.remove("device_user", params).catch(() => {});
  await service.remove("standard_user", params).catch(() => {});
  await service.remove("standard_organisation", params).catch(() => {});
  await service.remove("super_user", params).catch(() => {});
  await service.create(getPublicPrivilege(), params);
  await service.create(getDeviceUser(), params);
  await service.create(getStandardUser(), params);
  await service.create(getStandardOrganisations(), params);
  await service.create(getSuperUser(), params);
  console.log("Created privileges: standard_user, standard_organisations, super_user");
}

async function createUser(userId, email, privilegesAlias, app) {
  const params = { superUser: true };
  const request = {
    _id: userId,
    email,
    phone: "1-800-not-available",
    password: "685542d1-f239-4a5a-a386", // TODO: Place password in config file and k8s file.
    firstName: privilegesAlias,
    lastName: privilegesAlias,
    type: "user",
    privileges: [{ privilegesAlias, params: {} }],
  };

  const result = await app
    .service("users")
    .create(request, params)
    .catch((err) => {
      if (err.message.startsWith("E11000 duplicate key error collection")) {
        return app.service("users").update(userId, request, params);
      }
      throw err;
    });
  console.log("Created user: ", email);
  return result;
}

export default async (app) => {
  const result = await Promise.all([
    createPrivileges(app),
    createUser("c6e75342-2160-4703-94d8-b0bb291ea3c3", "system@lpgroup.com", "super_user", app),
    createUser(
      "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
      "system.import@lpgroup.com",
      "super_user",
      app
    ),
    createUser(
      "2127505c-6703-4985-9e98-5a8f483011a8",
      "system.nats@lpgroup.com",
      "super_user",
      app
    ),
    createUser("5a1f6ebb-5e8e-4158-add1-ebf391314cee", "anonymous@lpgroup.com", "public", app),
  ]);
  return { "system@lpgroup.com": result["1"] };
};
