# feathers-example-ng

REST api for feathers-example

## Install and run in development mode

```
// Install node 14.16 defined in .nvmrc
nvm use

// Install all npm packages
yarn

// Start service
yarn start
```

## Import testdata

In the folder /import/{test|production}

````
nvm use
yarn lpimport
```

## Endpoints

- lpgroup.com/api/v1/onboarding/standard-user
- lpgroup.com/api/v1/autenticate
- lpgroup.com/api/v1/users
- lpgroup.com/api/v1/organisations

````
