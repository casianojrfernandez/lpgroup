export async function registerUser(ax, request) {
  await ax.post("/onboarding/register-user", request, { ignoreError: true });
  return ax.login(request.email, request.password).then(async (o) => {
    return o.patch(`/users/${request._id}`, request);
  });
}

export async function addGrants(o, alias, userIds) {
  return Promise.all(
    userIds.map(async (userId) => {
      return o
        .post(`/organisations/${alias}/grants`, { userId }, { errorHandlerWithException: true })
        .catch((err) => {
          if (!err.message.startsWith("E11000 duplicate key error collection")) {
            throw err;
          }
        });
    })
  );
}
