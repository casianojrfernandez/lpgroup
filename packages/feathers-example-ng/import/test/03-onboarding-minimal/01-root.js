import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    ax.get("/", {
      expected: {
        authentication_url: "http://localhost:8081/authentication",
        countries_url: "http://localhost:8081/countries",
        organisation_url: "http://localhost:8081/organisations",
        users_url: "http://localhost:8081/users",
        vats_url: "http://localhost:8081/vats",
      },
    });
  });
};
