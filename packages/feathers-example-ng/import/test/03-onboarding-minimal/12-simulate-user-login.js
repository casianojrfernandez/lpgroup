import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    let userId;
    await ax.login().then(async (o) => {
      const user = await o.get("/users?email=onboarding@lpgroup.com");
      userId = user.data[0] && user.data[0]._id;
      if (!userId) throw new Error("User doesnt exist");
    });

    await ax.login("onboarding@lpgroup.com", "f56s3VNJz$=amrLnE").then(async (o) => {
      await o.get(`/users/not-exist-123`, {
        expected: {
          name: "NotFound",
          message: "No record found for id 'not-exist-123'",
          code: 404,
          className: "not-found",
          errors: {},
        },
      });

      // No permission to super user
      await o.get(`/users/620373d9-aba2-4566-9757-baa646dfacd1`, {
        expected: {
          name: "NotFound",
          message: "No record found for id '620373d9-aba2-4566-9757-baa646dfacd1'",
          code: 404,
          className: "not-found",
          errors: {},
        },
      });

      await o.patch(
        `/users/not-exist-123`,
        { phone: "not-exist-123" },
        {
          expected: {
            name: "NotFound",
            message: "No record found for id 'not-exist-123'",
            code: 404,
            className: "not-found",
            errors: {},
          },
        }
      );

      await o.patch(
        `/users/620373d9-aba2-4566-9757-baa646dfacd1`,
        { phone: "no-permission-for-this-super-user" },
        {
          expected: {
            name: "NotFound",
            message: "No record found for id '620373d9-aba2-4566-9757-baa646dfacd1'",
            code: 404,
            className: "not-found",
            errors: {},
          },
        }
      );

      await o.patch(
        `/users/${userId}`,
        { phone: "123 456 789" },
        {
          expected: {
            _id: "e6c40202-6977-4462-9813-a918a91d5afc",
            type: "user",
            active: true,
            phone: "123 456 789",
            lastName: "Custer",
            firstName: "Reverend",
            email: "onboarding@lpgroup.com",
            owner: {
              user: {
                _id: "e6c40202-6977-4462-9813-a918a91d5afc",
              },
            },
            changed: {
              by: "e6c40202-6977-4462-9813-a918a91d5afc",
              at: 1611064290940,
            },
            added: {
              by: "superuser",
              at: 1611064290704,
            },
            url: "http://localhost:8081/users/undefined",
          },
        }
      );

      await o.get(`/users/${userId}`, {
        expected: {
          _id: "282c91c1-72c6-4911-a8a8-91af7d55a5e0",
          type: "user",
          active: true,
          phone: "123 456 789",
          lastName: "Custer",
          firstName: "Reverend",
          email: "onboarding@lpgroup.com",
          owner: {
            user: {
              _id: "282c91c1-72c6-4911-a8a8-91af7d55a5e0",
            },
          },
          changed: {
            by: "282c91c1-72c6-4911-a8a8-91af7d55a5e0",
            at: 1611064351959,
          },
          added: {
            by: "superuser",
            at: 1611064351753,
          },
          url: "http://localhost:8081/users/undefined",
        },
      });

      // await o.get("/organisations", {
      //   expected: {
      //     total: 0,
      //     limit: 5000,
      //     skip: 0,
      //     data: [],
      //   },
      // });
    });
  });
};
