import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    let userId;
    await ax.login().then(async (o) => {
      const user = await o.get("/users?email=onboarding@lpgroup.com");
      userId = user.data[0] && user.data[0]._id;
      if (!userId) throw new Error("User doesnt exist");
      await o.remove("/organisations/cafe-minimal", { ignoreError: true });
    });

    await ax.login("onboarding@lpgroup.com", "f56s3VNJz$=amrLnE").then(async (o) => {
      const expected = {
        _id: "d9e0b1cc-4396-4d5f-83f2-9ddf46a4a2c1",
        alias: "cafe-minimal",
        name: "Cafe minimal",
        title: "Cafe minimal",
        images: [],
        theme: {
          color: {
            secondary: "#cccccc",
            primary: "#666666",
          },
        },
        location: {
          longitude: null,
          latitude: null,
        },
        visitAddress: {
          country: null,
          zipCode: null,
          state: null,
          city: null,
          street: null,
        },
        description: {
          text: null,
          short: null,
        },
        companyName: null,
        postAddress: {
          country: null,
          zipCode: null,
          state: null,
          city: null,
          street: null,
        },
        contact: {
          phone: null,
          email: null,
          firstName: null,
          lastName: null,
        },
        website: null,
        phone: null,
        email: null,
        organisationNumber: null,
        changed: {
          by: "8f7a39f4-8a78-4963-a049-591561fdeef0",
          at: 1614764095061,
        },
        added: {
          by: "8f7a39f4-8a78-4963-a049-591561fdeef0",
          at: 1614764095061,
        },
        owner: {
          user: {
            _id: "8f7a39f4-8a78-4963-a049-591561fdeef0",
          },
          organisation: {
            alias: "cafe-minimal",
          },
        },
        url: "http://localhost:8081/organisations/d9e0b1cc-4396-4d5f-83f2-9ddf46a4a2c1",
      };

      await o.post(
        "/organisations",
        {
          alias: "cafe-minimal",
          name: "Cafe minimal",
          title: "Cafe minimal",
        },
        { expected }
      );

      await o.get("/organisations");

      await o.get("/organisations/cafe-minimal", { expected });
      await o.get(`/users/${userId}`, {
        expected: {
          _id: "14530a71-05c1-4760-a23c-ca2171b1b5df",
          type: "user",
          active: true,
          phone: "123 456 789",
          lastName: "Custer",
          firstName: "Reverend",
          email: "onboarding@lpgroup.com",
        },
      });

      await o.get("/organisations/cafe-not-exist", {
        expected: {
          name: "NotFound",
          message: "No record found for id 'cafe-not-exist'",
          code: 404,
          className: "not-found",
          errors: {},
        },
      });

      await o.patch(
        "/organisations/cafe-minimal",
        {
          phone: "123-456",
        },
        {
          expected: { ...expected, phone: "123-456" },
        }
      );

      await o.put(
        "/organisations/cafe-minimal",
        {
          alias: "cafe-minimal",
          name: "Cafe minimal",
          title: "Cafe minimal",
        },
        {
          expected: { ...expected },
        }
      );
      await o.get(`/users/${userId}/permissions`, {
        expected: {
          "organisations": ["get", "update", "patch", "create"],
          "organisations/:organisationAlias/grants": [
            "get",
            "find",
            "create",
            "update",
            "patch",
            "remove",
          ],
          "users": ["get", "update", "patch"],
          "users/:userId/permissions": ["get", "find", "update", "patch"],
        },
      });
    });
  });
};
