import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    ax.get("/onboarding", {
      expected: {
        "register-user_url": "http://localhost:8081/onboarding/register-user",
      },
    });
  });
};
