import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    let userId;
    await ax.login().then(async (o) => {
      const user = await o.get("/users?email=onboarding@lpgroup.com");
      userId = user.data[0] ? user.data[0]._id : undefined;
      if (userId) await o.remove(`/users/${userId}`);
    });

    await ax.post(
      "/onboarding/register-user",
      {
        email: "onboarding@lpgroup.com",
        password: "f56s3VNJz$=amrLnE",
        firstName: "Reverend",
        lastName: "Custer",
      },
      {
        expected: {
          active: true,
          phone: null,
          lastName: "Custer",
          firstName: "Reverend",
          email: "onboarding@lpgroup.com",
          _id: "d3b7b6d0-80c9-4304-bcc0-7238c98ee549",
          type: "user",
          url: "http://localhost:8081/users/d3b7b6d0-80c9-4304-bcc0-7238c98ee549",
        },
      }
    );
  });
};
