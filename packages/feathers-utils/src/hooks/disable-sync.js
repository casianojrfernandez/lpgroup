import feathersSync from "feathers-sync";

const { SYNC } = feathersSync;

// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    context[SYNC] = false;
  };
};
