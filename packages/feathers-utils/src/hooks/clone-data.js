// Clone context.data, it will be used by validate-changes
// to verify that nothing has been changed.
//
import { cloneDeep } from "lodash-es";

export default (options = { name: "clone" }) => {
  return async (context) => {
    context.custom = context.custom || {};
    context.custom[options.name] = cloneDeep(context.data);
    return context;
  };
};
