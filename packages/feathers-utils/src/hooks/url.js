/* eslint-disable no-param-reassign */
/**
 * Add url key to json row.
 */
export default (options = { key: "_id" }) => {
  return async (context) => {
    // eslint-disable-next-line no-underscore-dangle
    if (context.params._calledByHook) return context;

    const { app, result } = context;
    if (app.get("mainDomain")) {
      console.error("Deprecated: mainDomain config");
    }
    const host = app.get("apiDomain") || app.get("mainDomain");

    const query = Object.entries(context.params.query || {});
    const url = query.reduce((accUrl, [key, value]) => {
      return accUrl.replace(`:${key}`, value);
    }, context.path);

    // Is it used by find or get
    const localResult = result.data ? result.data : [result];
    localResult.forEach((data) => {
      data.url = host.concat("/", url);
      if (options.key) data.url += `/${data[options.key]}`;
    });

    return context;
  };
};
