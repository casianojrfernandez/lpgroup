import { mergeWith, find } from "lodash-es";

function equalKeys(o1, o2, key) {
  return o1 && o2 && key in o1 && key in o2 && o1[key] === o2[key];
}

function customizer(objValue, srcValue) {
  if (Array.isArray(objValue) && Array.isArray(srcValue)) {
    for (let i = 0; i < srcValue.length; i += 1) {
      const value = srcValue[i];
      const objToUpdate = find(
        objValue,
        (o) => equalKeys(o, value, "_id") || equalKeys(o, value, "alias")
      );
      if (objToUpdate) {
        mergeWith(objToUpdate, value, customizer);
      } else {
        objValue.push(value);
      }
    }
    return objValue;
  }
  return undefined;
}

export function mergeData(source1, source2) {
  const target = {};
  mergeWith(target, source1, source2, customizer);
  return target;
}
