/**
 * Add the protocol that is used to params in all services.
 * @param {*} isASocket true - is a socket middleware
 *                      false - is a rest middleware
 */
export default (app, socket) => {
  if (socket) {
    return (req, next) => {
      req.feathers.protocol = "https";
      next();
    };
  }

  // Is a rest service
  return (req, res, next) => {
    req.feathers.protocol = req.protocol;
    next();
  };
};
