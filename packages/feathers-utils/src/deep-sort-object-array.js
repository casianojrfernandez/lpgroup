/* eslint-disable no-underscore-dangle */
function isObject(obj) {
  return obj && typeof obj === "object";
}

export function deepSortObjectArray(objArr, options = {}) {
  function hashObject(obj) {
    if (Array.isArray(obj)) {
      return JSON.stringify(obj);
    }
    if (isObject(obj)) {
      if (options.noArraySort && options.noArraySort === true) return 1;
      if (obj.alias) return obj.alias;
      if (obj._id) return obj._id;
      return JSON.stringify(
        Object.keys(obj)
          .sort()
          .filter((key) => {
            return !Array.isArray(obj[key]) && !isObject(obj[key]);
          })
          .map((key) => {
            return [key, obj[key]];
          })
      );
    }
    return obj;
  }

  function compareArrayObjects(a, b) {
    const aKey = hashObject(a);
    const bKey = hashObject(b);

    // eslint-disable-next-line no-nested-ternary
    return aKey < bKey ? -1 : aKey > bKey ? 1 : 0;
  }

  function _deepSortObjectArray(root) {
    if (Array.isArray(root)) {
      return root.map(_deepSortObjectArray).sort(compareArrayObjects);
    }

    if (isObject(root)) {
      return Object.keys(root)
        .sort()
        .reduce((sortedRoot, key) => {
          // eslint-disable-next-line no-param-reassign
          sortedRoot[key] = _deepSortObjectArray(root[key]);
          return sortedRoot;
        }, {});
    }

    return root;
  }

  return _deepSortObjectArray(objArr);
}
