import debug from "debug";
import express from "@feathersjs/express";

const dbgException = debug("exception");

export function errorHandler() {
  return express.errorHandler({
    logger: (err, str, req) => {
      console.error(`Error in ${req.method} ${req.url}`);
    },
    // eslint-disable-next-line consistent-return
    html: (error, req, res, next) => {
      if (res.headersSent) {
        return next(error);
      }
      if (error.code === "500") {
        dbgException(error);
      }
      res.status(error.code).send({ code: error.code, name: error.name, message: error.message });
    },
  });
}
