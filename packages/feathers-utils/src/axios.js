/**
 * Feathersjs wrapper of axios http client.
 */
import debugRoot from "debug";
import axios from "axios";
import rateLimit from "axios-rate-limit";
import errors from "@feathersjs/errors";
import { memoize } from "lodash-es";
import { checkExpected } from "./compare";
import { sleep } from "./common";

const debug = debugRoot("axios");
const debugError = debugRoot("axios:error");
const { NotFound, Timeout, GeneralError } = errors;

/**
 * setupAxios({baseURL: "http://cybercow.se", headers: { "X-Custom-Header": "LPGroup" }})
 */
const createOptions = {
  timeout: 60000,
  headers: { "X-Custom-Header": "lpGroup" },
};

let baseURL;
let errorHandlerWithException = true;
let maxRPS = 5;

// TODO: let requiredKeys = [];
let ignoreKeyCompare = [];

let globalUser;
let globalPassword;

function formatUrl(url) {
  return `${baseURL || ""}${url}`.toLowerCase();
}

export function setupAxios(options = {}) {
  if (options.baseURL) baseURL = options.baseURL.replace(/\/$/, "");
  if (options.timeout) createOptions.timeout = options.timeout;
  if (options.customHeader) createOptions.headers["X-Custom-Header"] = options.customHeader;
  if (options["x-api-key"]) createOptions.headers["x-api-key"] = options["x-api-key"];
  if (options.headers) createOptions.headers = { ...createOptions.headers, ...options.headers };

  if (options.maxRPS) maxRPS = options.maxRPS;

  if ("errorHandlerWithException" in options)
    errorHandlerWithException = options.errorHandlerWithException;

  // TODO: if ("requiredKeys" in options) requiredKeys = options.requiredKeys;
  if ("ignoreKeyCompare" in options) ignoreKeyCompare = options.ignoreKeyCompare;

  if ("user" in options) globalUser = options.user;
  if ("password" in options) globalPassword = options.password;
}

function getLoginInstance(fullUrl, rawInstance, options = {}) {
  return rawInstance.post(fullUrl, options).then((response) => {
    debug(`Logged in as ${response.data.user.email}`);
    return response.data.accessToken;
  });
}
const getLoginInstanceCached = memoize(getLoginInstance, (fullUrl, rawInstance, options) =>
  JSON.stringify(options)
);

// TODO: Change name?
// Other files are accessing this through, and that might be a bit confusing.
// const { instance } = require("@lpgroup/feathers-utils");
/**
 *
 * @param {*} instanceOptions
 * @param {*} reuseRawInstance An axios rateLimit object to reuse on this instance.
 */
export function instance(instanceOptions = {}, reuseRawInstance = undefined) {
  return {
    dbgIdentity: undefined,
    user: undefined,
    password: undefined,
    accessToken: undefined,
    rawInstance:
      reuseRawInstance ||
      rateLimit(axios.create({ ...createOptions, ...instanceOptions }), { maxRPS }),

    /** Returns axios instance with login headers */
    ins() {
      if (this.accessToken) {
        this.rawInstance.defaults.headers.common.Authorization = this.accessToken;
      } else {
        delete this.rawInstance.defaults.headers.common.Authorization;
      }
      return this.rawInstance;
    },

    // TOOD: Wait longer and longer
    async waitOnServerReady(url) {
      for (let x = 0; x < 100; x += 1) {
        try {
          // eslint-disable-next-line no-await-in-loop
          const result = await this.ins().get(url);
          if (result !== undefined && result.status === 200) {
            debug(`Connected to ${url}`);
            return;
          }
          // eslint-disable-next-line no-empty
        } catch (error) {}
        // eslint-disable-next-line no-await-in-loop
        await sleep(500);
        debugError(`  Wait on server ${url}`);
      }
      throw new Error("Server is not responding");
    },

    async closePlugin() {
      // do nothing.
    },

    async login(user, password) {
      const newInstance = instance(instanceOptions, this.rawInstance);
      newInstance.user = user || globalUser;
      newInstance.password = password || globalPassword;
      newInstance.dbgIdentity = newInstance.user;
      const fullUrl = formatUrl("/authentication");
      newInstance.accessToken = await getLoginInstanceCached(fullUrl, this.rawInstance, {
        strategy: "local",
        email: newInstance.user,
        password: newInstance.password,
      }).catch((err) => {
        newInstance.errorHandler("login", fullUrl)(err);
        throw err;
      });
      return newInstance;
    },

    async loginDevice(deviceId) {
      const newInstance = instance(instanceOptions, this.rawInstance);
      newInstance.dbgIdentity = deviceId;
      const fullUrl = formatUrl("/authentication");
      newInstance.accessToken = await getLoginInstanceCached(fullUrl, this.rawInstance, {
        strategy: "device",
        _id: deviceId,
      }).catch((err) => {
        newInstance.errorHandler("loginDevice", fullUrl)(err);
        throw err;
      });
      return newInstance;
    },

    async reauthenticate(accessToken) {
      const newInstance = instance(instanceOptions, this.rawInstance);
      const fullUrl = formatUrl("/authentication");
      newInstance.accessToken = await getLoginInstanceCached(fullUrl, this.rawInstance, {
        strategy: "jwt",
        accessToken,
      }).catch((err) => {
        newInstance.errorHandler("login", fullUrl)(err);
        throw err;
      });
      return newInstance;
    },

    async post(url, data, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .post(fullUrl, data)
        .then(this.responseHandler("post", fullUrl, options))
        .catch(this.errorHandler("post", fullUrl, options));
    },

    async get(url, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .get(fullUrl)
        .then(this.responseHandler("get", fullUrl, options))
        .catch(this.errorHandler("get", fullUrl, options));
    },

    async patch(url, data, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .patch(fullUrl, data)
        .then(this.responseHandler("patch", fullUrl, options))
        .catch(this.errorHandler("patch", fullUrl, options));
    },

    async put(url, data, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .put(fullUrl, data)
        .then(this.responseHandler("put", fullUrl, options))
        .catch(this.errorHandler("put", fullUrl, options));
    },

    async remove(url, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .delete(fullUrl)
        .then(this.responseHandler("delete", fullUrl, options))
        .catch(this.errorHandler("delete", fullUrl, options));
    },

    // TODO: Should handle other than pdf
    async downloadFile(url) {
      const fullUrl = formatUrl(url);
      const file = await this.ins()
        .get(url, {
          responseType: "arraybuffer",
          headers: {
            Accept: "application/pdf",
          },
        })
        .then(this.responseHandler("download", fullUrl))
        .catch(this.errorHandler("download", fullUrl));

      if (file) return file.toString("base64");
      throw Error(`Can't download file ${url}`);
    },

    async postPut(url, key, data) {
      return this.multiCallWithKey(this.postPutSingle, url, key, data);
    },

    async postPutSingle(url, key, data) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .post(fullUrl, data)
        .then(this.responseHandler("postPut", fullUrl))
        .catch(async (error) => {
          const { message } = error.response.data;
          if (message.startsWith("E11000 duplicate key error collection")) {
            const id = data[key];
            if (id) {
              return this.put(`${url}/${id}`, data);
            }
            debugError(`  POST: ${fullUrl} `);
            debugError(`    Missing ${key} in ${data}`);
          } else {
            this.errorHandler("postPut", fullUrl)(error);
          }
          return undefined;
        });
    },

    multiCallWithKey(func, id, key, data) {
      if (Array.isArray(data)) {
        return Promise.all(
          data.map(async (x) => {
            return func.call(this, id, key, x);
          })
        );
      }
      return func.call(this, id, key, data);
    },

    responseHandler(method, fullUrl, options = {}) {
      if (options.waitUntilExpected) return this.responseHandlerWithWait(method, fullUrl, options);
      return this.responseHandlerNoWait(method, fullUrl, options);
    },

    responseHandlerNoWait(method, fullUrl, options) {
      return (response) => {
        debug(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);
        checkExpected(response.data, { ignoreKeyCompare, ...options });
        return response.data;
      };
    },

    responseHandlerWithWait(method, fullUrl, options) {
      return async (response) => {
        if (options.waitUntilExpected > 1) process.stdout.write(".");
        else debug(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);

        if (
          checkExpected(response.data, { ignoreKeyCompare, ...options }) &&
          options.waitUntilExpected
        ) {
          await sleep(1000);
          return this.get(fullUrl, {
            ...options,
            waitUntilExpected: options.waitUntilExpected + 1,
          });
        }
        return response.data;
      };
    },

    errorHandler(method, fullUrl, options) {
      return (err) => {
        if (options && options.expected) {
          debug(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);
          if (err.response) checkExpected(err.response.data, { ignoreKeyCompare, ...options });
        } else if (errorHandlerWithException || (options && options.errorHandlerWithException)) {
          this.errorHandlerExceptions(method, fullUrl, err);
        } else if (options && options.ignoreError) {
          debug(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);
        } else {
          this.errorHandlerConsole(method, fullUrl, err);
        }
      };
    },

    errorHandlerConsole(method, fullUrl, err) {
      const data = (err.response && err.response.data) || "";
      debugError(
        `ErrorHandlerConsole: ${method}: ${fullUrl} ${err.message} (${JSON.stringify(
          data,
          null,
          " "
        )})`
      );
    },

    errorHandlerExceptions(method, fullUrl, err) {
      if (err.response && err.response.data) {
        if (err.response.data.code === 404) {
          throw new NotFound(err.response.data.message);
        } else if (err.response.data.name === "GeneralError") {
          throw new GeneralError(err.response.data.message, err.response.data.data);
        }
      } else if (err.code === "ECONNABORTED" || err.code === "ECONNRESET") {
        throw new Timeout("Internal timeout");
      } else if (err.code === "ECONNREFUSED") {
        debug(`Can't access remote server (${err.message})`);
        throw new GeneralError("Subsystem is down (see server logs).");
      }

      throw err;
    },
  };
}

export async function login(user, password) {
  const loginInstance = instance();
  return loginInstance.login(user, password);
}

export async function noLogin(options = {}) {
  return instance(options);
}
