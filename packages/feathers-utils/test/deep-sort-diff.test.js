import { deepSortDiff } from "../src";

describe("deep-sort-diff", () => {
  test("Complex array object sort on _id", async () => {
    const obj1 = {
      operations: [
        {
          _id: "8622a493-71a5-499f-bab5-f413714e58b5",
          texts: {
            DEFAULT: "Dokumentet **Twin sun** skapas",
          },
        },
        {
          _id: "6d83b48b-5d8d-4d79-98d6-7b32e9358867",
          texts: {
            DEFAULT: "Dokumentet **Black stars** skapas",
          },
        },
      ],
    };

    const obj2 = {
      operations: [
        {
          _id: "12b584f5-c2c9-4ea4-8bba-effa101dae75",
          texts: {
            DEFAULT: "Dokumentet **Twin sun** skapas",
          },
        },
        {
          _id: "398f14b8-09b1-4f3d-88db-41efcadb2abc",
          texts: {
            DEFAULT: "Dokumentet **Black stars** skapas",
          },
        },
      ],
    };

    const sorted = deepSortDiff(obj1, obj2);

    expect(sorted).toEqual([
      {
        kind: "E",
        path: ["operations", 1, "_id"],
        lhs: "8622a493-71a5-499f-bab5-f413714e58b5",
        rhs: "398f14b8-09b1-4f3d-88db-41efcadb2abc",
      },
      {
        kind: "E",
        path: ["operations", 1, "texts", "DEFAULT"],
        lhs: "Dokumentet **Twin sun** skapas",
        rhs: "Dokumentet **Black stars** skapas",
      },
      {
        kind: "E",
        path: ["operations", 0, "_id"],
        lhs: "6d83b48b-5d8d-4d79-98d6-7b32e9358867",
        rhs: "12b584f5-c2c9-4ea4-8bba-effa101dae75",
      },
      {
        kind: "E",
        path: ["operations", 0, "texts", "DEFAULT"],
        lhs: "Dokumentet **Black stars** skapas",
        rhs: "Dokumentet **Twin sun** skapas",
      },
    ]);
  });

  test("Complex array object unsorted", async () => {
    const obj1 = {
      operations: [
        {
          _id: "8622a493-71a5-499f-bab5-f413714e58b5",
          texts: {
            DEFAULT: "Dokumentet **Twin sun** skapas",
          },
        },
        {
          _id: "6d83b48b-5d8d-4d79-98d6-7b32e9358867",
          texts: {
            DEFAULT: "Dokumentet **Black stars** skapas",
          },
        },
      ],
    };

    const obj2 = {
      operations: [
        {
          _id: "12b584f5-c2c9-4ea4-8bba-effa101dae75",
          texts: {
            DEFAULT: "Dokumentet **Twin sun** skapas",
          },
        },
        {
          _id: "398f14b8-09b1-4f3d-88db-41efcadb2abc",
          texts: {
            DEFAULT: "Dokumentet **Black stars** skapas",
          },
        },
      ],
    };

    const sorted = deepSortDiff(obj1, obj2, { noArraySort: true });

    expect(sorted).toEqual([
      {
        kind: "E",
        path: ["operations", 1, "_id"],
        lhs: "6d83b48b-5d8d-4d79-98d6-7b32e9358867",
        rhs: "398f14b8-09b1-4f3d-88db-41efcadb2abc",
      },
      {
        kind: "E",
        path: ["operations", 0, "_id"],
        lhs: "8622a493-71a5-499f-bab5-f413714e58b5",
        rhs: "12b584f5-c2c9-4ea4-8bba-effa101dae75",
      },
    ]);
  });
});
