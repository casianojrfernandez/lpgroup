import { deepSortObjectArray } from "../src";

describe("deep-sort-object-arrays", () => {
  test("Simple array object", async () => {
    const myUnsortedObject = {
      b: 2,
      a: 1,
      d: [3, 5, 1],
      c: 3,
      e: [
        { f: 7, g: 8, h: 9, _id: 4 },
        { g: 2, h: 3, f: 1, _id: 2 },
        { g: 5, f: 4, h: 6, _id: 5 },
      ],
    };

    expect(deepSortObjectArray(myUnsortedObject)).toEqual({
      a: 1,
      b: 2,
      c: 3,
      d: [1, 3, 5],
      e: [
        { _id: 2, f: 1, g: 2, h: 3 },
        { _id: 4, f: 7, g: 8, h: 9 },
        { _id: 5, f: 4, g: 5, h: 6 },
      ],
    });
  });
  test("Complex array object", async () => {
    const myUnsortedObject = {
      total: 3,
      limit: 5000,
      skip: 0,
      data: [
        {
          _id: "8c045809-47d8-44eb-976c-442154438756",
          alias: "vat-12",
          title: "12% moms",
          description:
            "12 procent moms gäller för till exempel livsmedel, restaurangbesök, hotell och konstverk som konstnären säljer själv.",
          country: {
            alias: "se",
          },
          percentage: "0.11",
          changed: {
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
            at: 1606484229218,
          },
          added: {
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
            at: 1606484229218,
          },
          owner: {
            organisation: {
              _id: "superuser",
            },
          },
          url: "http://localhost:8081/vats/vat-12",
        },
        {
          _id: "d16c03c8-c051-4ced-afb2-2953584810b6",
          percentage: "0.06",
          country: {
            alias: "se",
          },
          description:
            "6 procent moms gäller till exempel för tidningar, böcker, taxi-, buss-, flyg- och tågresor i Sverige, konserter med mera. Skattesatsen 6 procent gäller ofta, men inte alltid, inom kulturområdet.",
          title: "6% moms",
          alias: "vat-06",
          changed: {
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
            at: 1606484229228,
          },
          added: {
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
            at: 1606484229228,
          },
          owner: {
            organisation: {
              _id: "superuser",
            },
          },
          url: "http://localhost:8081/vats/vat-06",
        },
        {
          _id: "3776846e-ab57-4ed3-ac46-41c6464360aa",
          alias: "vat-25",
          title: "25% moms",
          description:
            "25 procent moms gäller på alla varor och tjänster där ingen annan moms är angiven.",
          country: {
            alias: "se",
          },
          percentage: "0.20",
          changed: {
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
            at: 1606484229236,
          },
          added: {
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
            at: 1606484229236,
          },
          owner: {
            organisation: {
              _id: "superuser",
            },
          },
          url: "http://localhost:8081/vats/vat-25",
        },
      ],
    };
    const sorted = deepSortObjectArray(myUnsortedObject);
    expect(sorted).toEqual({
      data: [
        {
          _id: "d16c03c8-c051-4ced-afb2-2953584810b6",
          added: {
            at: 1606484229228,
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
          },
          alias: "vat-06",
          changed: {
            at: 1606484229228,
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
          },
          country: {
            alias: "se",
          },
          description:
            "6 procent moms gäller till exempel för tidningar, böcker, taxi-, buss-, flyg- och tågresor i Sverige, konserter med mera. Skattesatsen 6 procent gäller ofta, men inte alltid, inom kulturområdet.",
          owner: {
            organisation: {
              _id: "superuser",
            },
          },
          percentage: "0.06",
          title: "6% moms",
          url: "http://localhost:8081/vats/vat-06",
        },
        {
          _id: "8c045809-47d8-44eb-976c-442154438756",
          added: {
            at: 1606484229218,
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
          },
          alias: "vat-12",
          changed: {
            at: 1606484229218,
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
          },
          country: {
            alias: "se",
          },
          description:
            "12 procent moms gäller för till exempel livsmedel, restaurangbesök, hotell och konstverk som konstnären säljer själv.",
          owner: {
            organisation: {
              _id: "superuser",
            },
          },
          percentage: "0.11",
          title: "12% moms",
          url: "http://localhost:8081/vats/vat-12",
        },
        {
          _id: "3776846e-ab57-4ed3-ac46-41c6464360aa",
          added: {
            at: 1606484229236,
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
          },
          alias: "vat-25",
          changed: {
            at: 1606484229236,
            by: "37ef28d6-b4d5-49e7-bb42-450a1288c4be",
          },
          country: {
            alias: "se",
          },
          description:
            "25 procent moms gäller på alla varor och tjänster där ingen annan moms är angiven.",
          owner: {
            organisation: {
              _id: "superuser",
            },
          },
          percentage: "0.20",
          title: "25% moms",
          url: "http://localhost:8081/vats/vat-25",
        },
      ],
      limit: 5000,
      skip: 0,
      total: 3,
    });
  });
});
