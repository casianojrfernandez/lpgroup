import { setupAxios, instance, noLogin } from "../src";

describe("Axios", () => {
  test("In global scope", async () => {
    setupAxios({
      "baseURL": "https://api.thecatapi.com/v1/",
      "x-api-key": "446825e1-284d-4d4d-8fc7-7556636211ad",
    });

    const result = await instance().get("/breeds/search?q=bengal");
    expect(result[0].health_issues).toEqual(3);
  });

  test("NoLogin", async () => {
    setupAxios({
      "baseURL": "https://api.thecatapi.com/v1/",
      "x-api-key": "446825e1-284d-4d4d-8fc7-7556636211ad",
    });

    await noLogin().then(async (o) => {
      const result = await o.get("/breeds/search?q=bengal");
      expect(result[0].health_issues).toEqual(3);
    });
  });

  test("Singel login", async () => {
    setupAxios({
      "baseURL": "https://api.thecatapi.com/v1/",
      "x-api-key": "446825e1-284d-4d4d-8fc7-7556636211ad",
    });
    const result = await Promise.all([
      instance().get("/breeds/search?q=bengal"),
      instance().get("/breeds/search?q=toyger"),
    ]);

    expect(result[0][0].health_issues).toEqual(3);
    expect(result[1][0].health_issues).toEqual(2);
  });
});
