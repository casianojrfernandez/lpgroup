# @lpgroup/import-cli

[![npm version](https://badge.fury.io/js/%40lpgroup%2Fimport-cli.svg)](https://badge.fury.io/js/%40lpgroup%2Fimport-cli) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/import-cli/badge.svg)](https://snyk.io/test/npm/@lpgroup/import-cli)
[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Cli command that imports data into rest-api, RabbitMQ and NATS.

## Install

Installation of the npm

```sh
npm install @lpgroup/import-cli
```

## Configuration

.import.json

```json
{
  "environments": {
    "default": {
      "http": {
        "server": "https://api.thecatapi.com/v1/",
        "user": "",
        "password": "",
        "readyServer": "https://api.thecatapi.com/",
        "headers": { "x-api-key": "446825e1-284d-4d4d-8fc7-7556636211ad" }
      },
      "nats": { "uri": "nats://localhost:4222", "queue": "import-js", "messagePrefix": "int" },
      "rabbitmq": {
        "uri": "amqps://user:password@xxxx.cloudamqp.com/user",
        "queue": "dev"
      }
    }
  },
  "requiredKeys": ["owner", "added", "changed", "url"],
  "ignoreKeyCompare": ["_id", "owner", "added", "changed", "url"]
}
```

## Example

Add the following to `import/10-votes.js`.

```javascript
import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    await ax.post("/votes", {
      image_id: "good-cat",
      sub_id: "my-user-1234",
      value: 1,
    });
  });
};
```

Add the follwing script to `package.json`.

```json
{
  "scripts": {
    "import": "import-cli -v -e default -w import/"
  }
}
```

Run script from command line.

```bash
npm run import -- -i 10-votes
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
