/**
 * Helper functions that can be used by the import scripts
 */
import fs from "fs";
import { resolve } from "path";
import { PDFDocument } from "pdf-lib";

const { readFile } = fs.promises;

export { axios, nats, rabbitmq } from "./plugins";

/**
 * Load a pdf-file into a base64 uri string.
 */
export async function loadPdf(fileName) {
  const fullPath = resolve(fileName);
  const file = await readFile(fullPath);
  const pdf = await PDFDocument.load(file);
  const uri = await pdf.saveAsBase64({ dataUri: true });
  return uri;
}

/**
 * Load a pdf-file into a base64 uri string.
 */
export async function loadPdfToBase64(fileName) {
  const fullPath = resolve(fileName);
  const file = await readFile(fullPath);
  const uri = Buffer.from(file).toString("base64");
  return `data:application/pdf;base64,${uri}`;
}
