import {
  addPluginWithOptions,
  closePlugins,
  onPluginsReady,
  onPluginReady,
  axios as fpAxios,
  nats as fpNats,
  rabbitmq as fpRabbitmq,
} from "@lpgroup/feathers-plugins";

export { closePlugins };
export const axios = async () => onPluginReady("axios");
export const nats = async () => onPluginReady("nats");
export const rabbitmq = async () => onPluginReady("rabbitmq");

export function setupPlugins(config) {
  if (config.http.server)
    addPluginWithOptions("axios", fpAxios, {
      baseURL: config.http.server,
      customHeader: "import-cli",
      errorHandlerWithException: false,
      requiredKeys: config.requiredKeys,
      ignoreKeyCompare: config.ignoreKeyCompare,
      user: config.http.user,
      password: config.http.password,
      readyServer: config.http.readyServer,
      headers: config.http.headers,
      maxRPS: config.http.maxRPS || 200,
    });

  if (config.nats.uri)
    addPluginWithOptions("nats", fpNats, {
      name: "import-cli",
      uri: config.nats.uri,
      queue: config.nats.queue,
      messagePrefix: config.nats.messagePrefix,
      ignoreKeyCompare: config.ignoreKeyCompare,
    });

  if (config.rabbitmq.uri)
    addPluginWithOptions("rabbitmq", fpRabbitmq, {
      uri: config.rabbitmq.uri,
      queue: config.rabbitmq.queue,
    });
  return onPluginsReady();
}
