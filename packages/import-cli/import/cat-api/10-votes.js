// Projects using import-cli should have this dependency
// import { axios } from "@lpgroup/import-cli";
import { axios } from "../../src";

export default async () => {
  return axios().then(async (ax) => {
    await ax.post("/votes", {
      image_id: "asf2",
      sub_id: "my-user-1234",
      value: 1,
    });
  });
};
