// Projects using import-cli should have this dependency
// const { rabbitmq } = require("@lpgroup/import-cli");
import { rabbitmq } from "../../src";

export default async () => {
  return rabbitmq().then(async (rmq) => {
    // nc.subExpect("testing-nats", {
    //   expected: { name: "8a7a888d-d530-4947-8c3c-f27e55b8fe51" }
    // });

    await rmq.sentToQueue("lpgroup", "The Good Cow");

    // await nc.waitForSubExpect("testing-nats");
  });
};
