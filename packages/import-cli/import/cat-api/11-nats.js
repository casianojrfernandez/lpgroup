// Projects using import-cli should have this dependency
// const { nats } = require("@lpgroup/import-cli");
import { nats } from "../../src";

export default async () => {
  await nats().then(async (nc) => {
    nc.subExpect("testing-nats", {
      expected: { name: "8a7a888d-d530-4947-8c3c-f27e55b8fe51" },
    });

    await nc.pub("testing-nats", {
      name: "8a7a888d-d530-4947-8c3c-f27e55b8fe51",
    });

    await nc.waitForSubExpect("testing-nats");
  });
};
