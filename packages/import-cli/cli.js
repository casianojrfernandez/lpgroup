#!/usr/bin/env node --experimental-modules --es-module-specifier-resolution=node

// Needed by local packages installed with "npm link" to be able
// to find it's dependencies.
// TODO: Still need now when using lerna/yarn??
// if (process.env.NODE_ENV !== "PRODUCTION") {
//   // eslint-disable-next-line global-require
//   const path = require("path");
//   process.env.NODE_PATH = path.resolve("./node_modules");
//   // eslint-disable-next-line
//   require("module").Module._initPaths();
// }

// eslint-disable-next-line node/no-unpublished-bin
import debugSettings from "debug";
import { executeDirectory, generateWhiteList } from "@lpgroup/feathers-utils";
import { getCmdLineArgs, getConfig, printParameters } from "./src/parameters";
import { setupPlugins, closePlugins } from "./src/plugins";

async function main() {
  const parameters = getCmdLineArgs();
  const config = await getConfig(parameters.environment);
  console.time("Time");
  printParameters(parameters, config);

  if (parameters.verbose) {
    // Axios output to console.
    debugSettings.enable(`${debugSettings.load()},axios,axios:error,nats,rabbitmq,compare`);
    debugSettings.save();
  }

  // The script can handle one server of each type
  setupPlugins(config).then(() => {
    const options = {
      include: generateWhiteList(parameters.extension, parameters.include, parameters.exclude),
      verbose: parameters.verbose,
    };
    executeDirectory(parameters.cwd, options).then(() => {
      // eslint-disable-next-line no-console
      console.log("Done");
      console.timeEnd("Time");
      closePlugins();
    });
  });
}

main().catch((error) => {
  console.error(error.message);
  console.error(error);
});
