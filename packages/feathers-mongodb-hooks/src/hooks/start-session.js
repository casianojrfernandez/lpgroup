import { setClient, setDatabase, startSession, reuseSession, debugMsg } from "../sessions";

/**
 * Start a mongodb session and creates a new sessionId in params
 *
 * @param {*} options {client, database}
 */
export default (options = {}) => {
  setClient(options.client);
  setDatabase(options.database);

  return async (context) => {
    const { params } = context;

    if (params.sessionId) {
      reuseSession(params.sessionId);
      debugMsg("reuseSession", context);
    } else {
      const { sessionId, session } = await startSession();
      params.sessionId = sessionId;
      params.mongodb = { session };
      debugMsg("startSession", context);
    }
    return context;
  };
};
