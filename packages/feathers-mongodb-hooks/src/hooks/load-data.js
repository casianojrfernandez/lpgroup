import errors from "@feathersjs/errors";
import { startGetAndLockTransaction } from "../sessions";

export default (options = {}) =>
  async (context) => {
    const { data } = context;

    if (context.method === "update") {
      const dbData = await startGetAndLockTransaction(context, options.collections);

      if (!dbData) {
        throw new errors.NotFound(`No record found for id '${context.id}'`);
      }

      const keysToCopy = [
        ...Object.keys(context.service.options.schema.dbSchema),
        "myLock",
        "_id",
        "alias",
      ];
      keysToCopy.forEach((field) => {
        if (dbData[field]) data[field] = dbData[field];
      });
    }

    return context;
  };
