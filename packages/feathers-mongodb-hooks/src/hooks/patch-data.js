import errors from "@feathersjs/errors";
import { mergeData } from "@lpgroup/feathers-utils";
import { startGetAndLockTransaction } from "../sessions";

// eslint-disable-next-line no-unused-vars
export default (options = {}) =>
  async (context) => {
    if (context.method === "patch") {
      const dbData = await startGetAndLockTransaction(context, options.collections);
      if (!dbData) {
        throw new errors.NotFound(`No record found for id '${context.id}'`);
      }

      context.data = mergeData(dbData, context.data);
    }

    return context;
  };
