/* eslint-disable no-undef */
/* eslint-disable no-underscore-dangle */
import mongodb from "mongodb";
import service from "feathers-mongodb";
import feathers from "@feathersjs/feathers";
import { cloneDeep, omit } from "lodash-es";
import { sleep } from "@lpgroup/feathers-utils";
import { getSessionCounter } from "../src/index";
import { startSession, endSession, errorSession, lockData } from "../src/hooks";

const mongoUrl =
  process.env.MONGODB || "mongodb://localhost:27017,localhost:27018,localhost:27019/feathers-test";
// eslint-disable-next-line no-unused-vars
const detailsAfterHook = (options = {}) => {
  return async (context) => {
    const { params, app } = context;
    const { log } = context.data;
    const last = log[log.length - 1];
    await app.service("people").patch(context.id, { last }, params);
    return context;
  };
};

describe("Feathers MongoDB Service - Transactions", () => {
  const app = feathers();

  let db;
  let mongoClient;

  beforeAll(async () => {
    mongoClient = await mongodb.MongoClient.connect(mongoUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoClient.db("feathers-test");
    await Promise.all([
      db.collection("people").removeMany(),
      db.collection("people_details").removeMany(),
    ]);

    // Setup people service
    app.use("/people", service({ events: ["testing"] }));
    const people = app.service("people");
    people.Model = db.collection("people");
    people.hooks({
      before: { all: [startSession({ client: mongoClient, database: db })] },
      after: { all: [endSession()] },
      error: { all: [errorSession()] },
    });

    // Setup people details service
    app.use("/people_details", service({ events: ["testing"] }));
    const peopleDetails = app.service("people_details");
    peopleDetails.Model = db.collection("people_details");
    peopleDetails.hooks({
      before: { all: [startSession({ client: mongoClient, database: db })] },
      after: { all: [endSession()], patch: [detailsAfterHook()] },
      error: { all: [errorSession()] },
    });
  });

  afterAll(async () => db.dropDatabase().then(() => mongoClient.close()));

  describe("Transactions", () => {
    let peopleService;
    let peopleDetailsService;
    let people;
    let startSessionHook;
    let endSessionHook;
    let errorSessionHook;
    let lockDataHook;
    let context;
    let name;

    beforeEach(async () => {
      // Setup hooks
      startSessionHook = startSession({ client: mongoClient, database: db });
      endSessionHook = endSession();
      errorSessionHook = errorSession();
      lockDataHook = lockData({
        collections: [{ collection: "people", query: "name", field: "name" }],
      });

      // Setup test data
      name = "Ryan";
      context = {
        method: "post",
        path: "people",
        params: { query: { name } },
      };

      // Create people test data
      peopleService = app.service("/people");
      people = await Promise.all([
        peopleService.create({ name, age: 0 }),
        peopleService.create({ name: "AAA" }),
        peopleService.create({ name: "aaa" }),
        peopleService.create({ name: "ccc" }),
      ]);

      // Create people_details test data
      peopleDetailsService = app.service("/people_details");
      const { _id } = people[0];
      await Promise.all([
        peopleDetailsService.create({ _id, name, age: 0, log: [] }),
        peopleDetailsService.create({ _id: people[1]._id, log: [] }),
        peopleDetailsService.create({ _id: people[2]._id, log: [] }),
        peopleDetailsService.create({ _id: people[3]._id, log: [] }),
      ]);
    });

    afterEach(async () => {
      if (people) {
        await Promise.all([
          peopleService.remove(people[0]._id),
          peopleService.remove(people[1]._id),
          peopleService.remove(people[2]._id),
          peopleService.remove(people[3]._id),
          peopleDetailsService.remove(people[0]._id),
          peopleDetailsService.remove(people[1]._id),
          peopleDetailsService.remove(people[2]._id),
          peopleDetailsService.remove(people[3]._id),
        ]).catch();
      }
    });

    it("simple nested start/stop session", async () => {
      const { params } = context;

      await startSessionHook(context);
      expect(getSessionCounter(params.sessionId)).toBe(1);

      await startSessionHook(context);
      expect(getSessionCounter(params.sessionId)).toBe(2);

      await startSessionHook(context);
      expect(getSessionCounter(params.sessionId)).toBe(3);

      const person = await peopleService.create({ name });
      const results = await peopleService.find({
        query: {
          _id: new mongodb.ObjectID(person._id),
        },
      });

      await endSessionHook(context);
      expect(getSessionCounter(params.sessionId)).toBe(2);

      await endSessionHook(context);
      expect(getSessionCounter(params.sessionId)).toBe(1);

      await endSessionHook(context);
      expect(() => getSessionCounter(params.sessionId)).toThrowError(
        "Transaction aborted (getSessionObject: Session doesn't exist) sessionId undefined"
      );

      expect(results).toHaveLength(1);

      await peopleService.remove(person._id);
    });

    // eslint-disable-next-line func-names
    it("transactions timeout", async () => {
      const localContext = cloneDeep(context);
      const { params } = localContext;
      try {
        await startSessionHook(localContext);
        await lockDataHook(localContext);
        const peopleRyan = (await peopleService.find(params))[0];
        peopleRyan.timeoutTest = "timeout";
        await peopleService.patch(peopleRyan._id, peopleRyan, params);
        await sleep(90);

        await endSessionHook(localContext);
        expect(() => getSessionCounter(params.sessionId)).toThrowError(
          "Transaction aborted (getSessionObject: Session doesn't exist) sessionId undefined"
        );
      } catch (err) {
        localContext.error = { message: err.message };
        await errorSessionHook(localContext);
      }
    }, 20000);

    it("nested start/stop transactions", async () => {
      async function doTrans() {
        const localContext = cloneDeep(context);
        const { params } = localContext;
        try {
          await startSessionHook(localContext);
          await lockDataHook(localContext);

          const peopleRyan = (await peopleService.find(params))[0];
          const peopleDetails = await peopleDetailsService.get(peopleRyan._id, params);

          peopleRyan.age += 1;
          await peopleService.patch(peopleRyan._id, peopleRyan, params);

          // Need to update people before peopleDetails to get the hook to work.
          peopleDetails.log.push(omit(peopleRyan, ["last", "myLock"]));
          await peopleDetailsService.patch(peopleDetails._id, peopleDetails, params);

          await endSessionHook(localContext);
        } catch (err) {
          localContext.error = { message: err.message };
          await errorSessionHook(localContext);
        }
      }

      const { query } = context.params;
      const allPromieses = [];
      const numberOfTests = 10;
      for (let age = 0; age < numberOfTests - 1; age += 1) {
        allPromieses.push(doTrans());
      }
      await Promise.all(allPromieses);
      await doTrans();
      const peopleRyan = (await peopleService.find({ query }))[0];
      expect(peopleRyan).toEqual(
        expect.objectContaining({
          _id: expect.anything(),
          name: expect.any(String),
          age: expect.any(Number),
          last: expect.objectContaining({
            _id: expect.anything(),
            name: expect.any(String),
            age: expect.any(Number),
          }),
          myLock: expect.anything(),
        })
      );
      expect(peopleRyan.age).toBe(numberOfTests);
      expect(peopleRyan.last.name).toBe(name);

      const peopleDetails = await peopleDetailsService.get(peopleRyan._id);
      expect(peopleDetails.log).toHaveLength(numberOfTests);
    });
  });
});
