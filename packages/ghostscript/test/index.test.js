import fs from "fs";
import { fileURLToPath } from "url";
import { dirname } from "path";
import jsBase64 from "js-base64";
import { optimize } from "../src/index";

const { readFile, writeFile, unlink } = fs.promises;
const { encode } = jsBase64;
// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

describe("Ghostscript", () => {
  let input1;
  let input2;
  const fileIn1 = `${__dirname}/flytt_uppdrag.pdf`;
  const fileOut1 = `${__dirname}/flytt_uppdrag_optimized.pdf`;

  const fileIn2 = `${__dirname}/pdf-1.6.pdf`;
  const fileOut2 = `${__dirname}/pdf-1.6_optimized.pdf`;

  beforeAll(async () => {
    input1 = await readFile(fileIn1);
    input2 = await readFile(fileIn2);
  });

  beforeEach(async () => {
    await Promise.all([unlink(fileOut1), unlink(fileOut2)]).catch(() => {});
  });

  test("Optimize golden", async () => {
    const output = await optimize({ input: input1 });
    await writeFile(fileOut1, output, "binary");
    expect(output.length).toBeGreaterThan(45000);
    expect(output.length).toBeLessThan(670000);
  });

  test("File with some errors in", async () => {
    const output = await optimize({ input: input2 });
    await writeFile(fileOut2, output, "binary");
    expect(output.length).toBeGreaterThan(45000);
    expect(output.length).toBeLessThan(670000);
  });

  test("Optimize error 1", async () => {
    const output = await optimize({ input: input1 });
    const base64 = `data:application/pdf;base64,${encode(output)}`;
    await expect(optimize({ input: base64 })).rejects.toThrow(Error);
  });

  // TODO: This test fails.
  // eslint-disable-next-line jest/no-commented-out-tests
  // test("Optimize error 2", async () => {
  //   return expect(optimize({ input: "asdfasdfasdfasdf" })).rejects.toEqual(
  //     new Error("Can't optimize pdf GPL Ghostscript 9.52: Unrecoverable error, exit code 1")
  //   );
  // });
});
