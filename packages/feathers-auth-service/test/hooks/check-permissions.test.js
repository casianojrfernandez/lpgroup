import { checkPermissions, buildUserPrivileges } from "../../src/hooks/check-permissions";

describe("check-permissions", () => {
  describe("Super user", () => {
    const privileges = {
      data: [
        {
          alias: "super_user",
          name: "Super user",
          permissions: {
            users: {
              query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
              methods: ["*"],
            },
          },
        },
      ],
    };

    const context = {
      path: "users",
      method: "get",
      params: {
        user: {
          privileges: [
            { privilegesAlias: "super_user", params: { userId: "*", organisationAlias: "*" } },
          ],
        },
      },
    };
    const userPrivileges = buildUserPrivileges(context, "public");

    test("Super user get", async () => {
      expect(checkPermissions({ ...context, method: "get" }, userPrivileges, privileges)).toEqual(
        {}
      );
    });

    test("Super user post", async () => {
      expect(checkPermissions({ ...context, method: "post" }, userPrivileges, privileges)).toEqual(
        {}
      );
    });

    test("Super user missing privileges", async () => {
      expect(() => {
        checkPermissions({ ...context, path: "none-existing" }, userPrivileges, { data: [] });
      }).toThrowError("Access denied get none-existing");
    });

    test("Super user missing service", async () => {
      expect(() => {
        checkPermissions({ ...context, path: "none-existing" }, userPrivileges, privileges);
      }).toThrowError("Access denied get none-existing");
    });
  });

  describe("No user", () => {
    const privileges = {
      data: [
        {
          alias: "public",
          permissions: {
            users: {
              query: [{ key: "_id", param: "*" }],
              methods: ["get"],
            },
            usersWithId: {
              query: [{ key: "_id", param: "anId" }],
              methods: ["get"],
            },
          },
        },
      ],
    };

    const context = {
      path: "users",
      method: "get",
      params: {
        user: {
          email: "user@lpgroup.com",
          privileges: [],
        },
      },
    };
    const userPrivileges = buildUserPrivileges(context, "public");

    test("No user - get", async () => {
      expect(checkPermissions({ ...context }, userPrivileges, privileges)).toEqual({});
    });

    test("No user - get with a id", async () => {
      expect(
        checkPermissions({ ...context, path: "usersWithId" }, userPrivileges, privileges)
      ).toEqual({
        _id: "anId",
      });
    });

    test("No user - missing privileges", async () => {
      expect(() => {
        checkPermissions({ ...context, path: "users" }, userPrivileges, { data: [] });
      }).toThrowError("Access denied get users");
    });

    test("No user - missing service", async () => {
      expect(() => {
        checkPermissions({ ...context, path: "none-existing" }, userPrivileges, privileges);
      }).toThrowError("Access denied get none-existing");
    });
  });

  describe("Standard user", () => {
    const privileges = {
      data: [
        {
          alias: "standard_user",
          name: "Standard user",
          permissions: {
            onlyThisUser: {
              query: [{ key: "_id", param: "userId" }],
              methods: ["get", "update", "patch"],
            },
            users: {
              query: [{ key: "_id", param: "userId" }],
              methods: ["get", "update", "patch"],
            },
          },
        },
        {
          alias: "standard_organisation",
          name: "Standard organisation",
          permissions: {
            users: {
              query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
              methods: ["get", "find", "patch"],
            },
          },
        },
      ],
    };

    const context = {
      path: "users",
      method: "get",
      params: {
        user: {
          email: "user@lpgroup.com",
          privileges: [
            {
              privilegesAlias: "standard_user",
              params: { userId: "user-1" },
            },
            {
              privilegesAlias: "standard_organisation",
              params: { userId: "user-1", organisationAlias: "org-1" },
            },
            {
              privilegesAlias: "standard_organisation",
              params: { userId: "user-1", organisationAlias: "org-2" },
            },
          ],
        },
      },
    };
    const userPrivileges = buildUserPrivileges(context, "public");

    test("Standard get onlyThisUser", async () => {
      expect(
        checkPermissions({ ...context, path: "onlyThisUser" }, userPrivileges, privileges)
      ).toEqual({
        _id: "user-1",
      });
    });

    test("Standard get user", async () => {
      expect(checkPermissions({ ...context, method: "get" }, userPrivileges, privileges)).toEqual({
        $or: [{ _id: "user-1" }, { "owner.organisation.alias": { $in: ["org-1", "org-2"] } }],
      });
    });

    test("Standard patch", async () => {
      expect(checkPermissions({ ...context, method: "patch" }, userPrivileges, privileges)).toEqual(
        {
          $or: [{ _id: "user-1" }, { "owner.organisation.alias": { $in: ["org-1", "org-2"] } }],
        }
      );
    });

    test("Standard missing privileges", async () => {
      expect(() => {
        checkPermissions({ ...context, path: "none-existing" }, userPrivileges, { data: [] });
      }).toThrowError("Access denied get none-existing");
    });

    test("Standard missing service", async () => {
      expect(() => {
        checkPermissions({ ...context, path: "none-existing" }, userPrivileges, privileges);
      }).toThrowError("Access denied get none-existing");
    });
  });

  describe("Two organisations", () => {
    const privileges = {
      data: [
        {
          alias: "standard_organisation",
          name: "Standard organisation",
          permissions: {
            organisations: {
              query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
              methods: ["get", "find", "patch"],
            },
          },
        },
        {
          alias: "readonly_organisation",
          name: "Readonly organisation",
          permissions: {
            organisations: {
              query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
              methods: ["get", "find"],
            },
          },
        },
      ],
    };

    const context = {
      path: "organisations",
      method: "get",
      params: {
        user: {
          email: "user@lpgroup.com",
          privileges: [
            {
              privilegesAlias: "standard_organisation",
              params: { userId: "user-1", organisationAlias: "org-1" },
            },
            {
              privilegesAlias: "readonly_organisation",
              params: { userId: "user-1", organisationAlias: "org-2" },
            },
          ],
        },
      },
    };
    const userPrivileges = buildUserPrivileges(context, "public");

    test("Two organisations get", async () => {
      expect(checkPermissions({ ...context, method: "get" }, userPrivileges, privileges)).toEqual({
        "owner.organisation.alias": { $in: ["org-1", "org-2"] },
      });
    });

    test("Two organisations patch", async () => {
      expect(checkPermissions({ ...context, method: "patch" }, userPrivileges, privileges)).toEqual(
        {
          "owner.organisation.alias": "org-1",
        }
      );
    });
  });

  describe("Build privilegies", () => {
    const context = {
      path: "organisations",
      method: "get",
      params: {
        user: {
          email: "user@lpgroup.com",
          privileges: [
            {
              privilegesAlias: "standard_organisation",
              params: { userId: "user-1", organisationAlias: "org-1" },
            },
            {
              privilegesAlias: "readonly_organisation",
              params: { userId: "user-1", organisationAlias: "org-2" },
            },
          ],
        },
      },
    };

    test("Build privilegies", async () => {
      expect(buildUserPrivileges(context, "public")).toEqual([
        {
          privilegesAlias: "standard_organisation",
          params: { userId: "user-1", organisationAlias: "org-1" },
        },
        {
          privilegesAlias: "readonly_organisation",
          params: { userId: "user-1", organisationAlias: "org-2" },
        },
        {
          privilegesAlias: "public",
          params: {},
        },
      ]);
    });
  });
});
