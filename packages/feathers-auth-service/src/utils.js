const cachedUsers = [];
export async function getCachedUser(app, email) {
  if (!cachedUsers[email]) {
    const userData = await app.service("users").find({ limit: 1, query: { email } });

    // eslint-disable-next-line prefer-destructuring
    cachedUsers[email] = userData.data[0];
  }
  return cachedUsers[email];
}
