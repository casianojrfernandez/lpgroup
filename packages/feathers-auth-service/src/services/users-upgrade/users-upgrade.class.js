import { internalParams } from "@lpgroup/feathers-utils";

export class UsersUpgrade {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async create(data, params) {
    const { userId } = params.query;
    const userData = {
      ...data,
      _id: userId,
      type: "user",
      privileges: [{ privilegesAlias: "standard_user", params: { userId } }],
    };
    return this.app
      .service("users")
      .patch(userId, userData, internalParams({ ...params, superUser: true }));
  }
}
