import * as yup from "@lpgroup/yup";

const requestSchema = {
  _id: yup.id(),
  userId: yup.uuid().required(),
};

const dbSchema = {
  organisationAlias: yup.labelText().required(),
  added: yup.changed(),
  changed: yup.changed(),
  owner: yup.owner(),
};

export default yup.buildValidationSchema(requestSchema, dbSchema);
