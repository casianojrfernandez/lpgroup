// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    const { data, params } = context;
    data.organisationAlias = params.query.organisationAlias;
    data.owner = { organisation: { alias: params.query.organisationAlias } };
    return context;
  };
};
