import { Service } from "feathers-mongodb";
import { internalParams } from "@lpgroup/feathers-utils";
import { onPluginReady } from "@lpgroup/feathers-plugins";

function userHasPrivilege(user, privilegeAlias, organisationAlias) {
  return user.privileges.find(
    (v) => v.privilegesAlias === privilegeAlias && v.params.organisationAlias === organisationAlias
  );
}

function removePrivilege(user, privilegeAlias, organisationAlias) {
  return user.privileges.filter(
    (v) =>
      !(v.privilegesAlias === privilegeAlias && v.params.organisationAlias === organisationAlias)
  );
}

async function removePrivilegeFromUser(userId, params, app) {
  const { organisationAlias } = params.query;
  const localParams = internalParams(params, {}, { clearSession: true });
  const user = await app.service("users").get(userId, localParams);
  const requestUser = app.service("users").options.schema.removeDbSchemaKeys(user);
  requestUser.privileges = removePrivilege(user, "standard_organisation", organisationAlias);
  await app.service("users").patch(userId, { privileges: requestUser.privileges }, localParams);
}

export class OrganisationGrants extends Service {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("organisation-grants");
      database.createIndexThrowError(
        "organisation-grants",
        { organisationAlias: 1, userId: 1 },
        { unique: true }
      );
    });
  }

  // TODO: This should probably be in a transaction
  async create(data, params) {
    const result = await super.create(data, params);
    const { organisationAlias, userId } = data;

    const user = await this.app.service("users").get(userId, internalParams(params));
    if (!userHasPrivilege(user, "standard_organisation", organisationAlias)) {
      await this.app.service("users").patch(
        userId,
        {
          privileges: [
            { privilegesAlias: "standard_organisation", params: { userId, organisationAlias } },
          ],
        },
        internalParams(params, {}, { clearSession: true })
      );
    }
    return result;
  }

  async remove(userId, params) {
    const result = await super.remove(userId, params);
    const resultArr = Array.isArray(result) ? result : [result];
    await Promise.all(
      resultArr.map(async (grant) => removePrivilegeFromUser(grant.userId, params, this.app))
    );
    return result;
  }
}
