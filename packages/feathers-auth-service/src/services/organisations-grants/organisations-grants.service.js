import { OrganisationGrants } from "./organisations-grants.class";
import hooks from "./organisations-grants.hooks";
import schema from "./organisations-grants.yup";

export default (app) => {
  const options = {
    schema,
    paginate: app.get("paginate"),
    multi: ["remove"],
  };
  app.use("/organisations/:organisationAlias/grants", new OrganisationGrants(options, app));
  const service = app.service("organisations/:organisationAlias/grants");
  service.hooks(hooks);
};
