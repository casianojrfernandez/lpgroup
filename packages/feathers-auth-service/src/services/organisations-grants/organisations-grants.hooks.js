import feathersHooksCommon from "feathers-hooks-common";
import authentication from "@feathersjs/authentication";
import { validateRequest, validateDatabase } from "@lpgroup/yup";
import { url } from "@lpgroup/feathers-utils/hooks";
// eslint-disable-next-line import/no-named-default
import { default as checkPermissions } from "../../hooks/check-permissions";
import changed from "../../hooks/changed";
import setOwner from "./hooks/set-owner-organisation";

const { authenticate } = authentication.hooks;
const { disallow, iff, isProvider } = feathersHooksCommon;

export default {
  before: {
    all: [authenticate("jwt"), iff(isProvider("external"), checkPermissions())],
    find: [],
    get: [],
    create: [validateRequest(), setOwner(), changed(), validateDatabase()],
    update: [disallow()],
    patch: [disallow()],
    remove: [],
  },

  after: {
    all: [url({ key: "_id" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
