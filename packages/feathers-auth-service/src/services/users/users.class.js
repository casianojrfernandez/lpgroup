import { Service } from "feathers-mongodb";
import { internalParams } from "@lpgroup/feathers-utils";
import { onPluginReady } from "@lpgroup/feathers-plugins";

async function removeOrganisationGrants(userId, privileges, params, app) {
  return Promise.all([
    privileges
      .filter((v) => v.privilegesAlias === "standard_organisation")
      .map((privilege) => {
        return app
          .service("organisations/:organisationAlias/grants")
          .remove(
            userId,
            internalParams(params, { organisationAlias: privilege.params.organisationAlias })
          )
          .catch((err) => {
            if (err.code !== 404) throw err;
          });
      }),
  ]);
}

export class Users extends Service {
  constructor(options, app) {
    super(options);
    this.app = app;
    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("users");
      database.createIndexThrowError("users", { email: 1 }, { unique: true });
    });
  }

  async remove(userId, params) {
    const result = await super.remove(userId, params);
    const resultArr = Array.isArray(result) ? result : [result];
    await Promise.all(
      resultArr.map(async (user) =>
        removeOrganisationGrants(user._id, user.privileges, params, this.app)
      )
    );
    return result;
  }
}
