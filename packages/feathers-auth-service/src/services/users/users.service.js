import { mergeValidationsSchema } from "@lpgroup/yup";
import { Users } from "./users.class";
import hooks from "./users.hooks";
import schema from "./users.yup";

export default (app, optionsOverride) => {
  const options = {
    id: "_id",
    paginate: app.get("paginate"),
    schema: mergeValidationsSchema(schema, optionsOverride.schema),
  };
  if (optionsOverride.Users) {
    app.use("/users", new optionsOverride.Users(options, app));
  } else {
    app.use("/users", new Users(options, app));
  }
  const service = app.service("users");
  service.hooks(hooks);
};

export { Users };
