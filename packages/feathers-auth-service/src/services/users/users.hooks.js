/* eslint-disable import/no-named-default */
import debugRoot from "debug";
import feathersHooksCommon from "feathers-hooks-common";
import authHooks from "@feathersjs/authentication";
import authLocalHooks from "@feathersjs/authentication-local";
import { validReq, validDB } from "@lpgroup/yup";
import { patchData, loadData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import changed from "../../hooks/changed";
import { default as checkPermissions } from "../../hooks/check-permissions";
import owner from "./hooks/set-owner-user";
import signIn from "./hooks/set-signin";

const debug = debugRoot("auth");
const { discard, iff, isProvider } = feathersHooksCommon;
const { authenticate } = authHooks;
const hash = authLocalHooks.hooks.hashPassword;

const debugLoginError = (context) => {
  debug(`Invalid user/JWT ${context.id} ${context.error.message}`);
};

export default {
  before: {
    // Don't check permissions for internal calls.
    all: [authenticate("jwt"), iff(isProvider("external"), checkPermissions())],
    find: [],
    get: [],
    create: [hash("password"), validReq(), owner(), signIn(), changed(), validDB()],
    update: [hash("password"), validReq(), loadData(), owner(), signIn(), changed(), validDB()],
    patch: [hash("password"), validReq(), patchData(), owner(), signIn(), changed(), validDB()],
    remove: [],
  },

  after: {
    // Make sure the password field is never sent to the client
    // Always must be the last hook
    all: [
      url({ key: "_id" }),
      iff(isProvider("external"), discard("password", "privileges", "signInCode", "signInEnabled")),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [debugLoginError],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
