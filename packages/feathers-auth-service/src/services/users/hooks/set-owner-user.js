// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    const { data } = context;
    data.owner = { user: { _id: data._id } };
    return context;
  };
};
