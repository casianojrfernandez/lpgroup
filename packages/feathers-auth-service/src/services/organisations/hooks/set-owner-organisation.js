// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    const { data } = context;
    data.owner.organisation = { alias: data.alias };
    return context;
  };
};
