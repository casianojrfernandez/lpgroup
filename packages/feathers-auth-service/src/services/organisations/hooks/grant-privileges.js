import { internalParams } from "@lpgroup/feathers-utils";

// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    try {
      await context.app
        .service("organisations/:organisationAlias/grants")
        .create(
          { userId: context.params.user._id },
          internalParams(context.params, { organisationAlias: context.data.alias })
        );
    } catch (err) {
      if (!err.message.startsWith("E11000 duplicate key error collection")) {
        throw err;
      }
    }
    return context;
  };
};
