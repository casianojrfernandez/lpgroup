import { onPluginReady } from "@lpgroup/feathers-plugins";
import { internalParams } from "@lpgroup/feathers-utils";
import { Service } from "feathers-mongodb";

export class Organisations extends Service {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("organisations");
      database.createIndexThrowError("organisations", { alias: 1 }, { unique: true });
    });
  }

  async remove(id, params) {
    const result = await super.remove(id, params);

    // Cascade delete
    const resultArr = Array.isArray(result) ? result : [result];
    await Promise.all(
      resultArr.map(async (organisation) =>
        this.app
          .service("organisations/:organisationAlias/grants")
          .remove(null, internalParams(params, { organisationAlias: organisation.alias }))
      )
    );

    return result;
  }
}
