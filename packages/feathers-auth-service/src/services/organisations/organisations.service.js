import { mergeValidationsSchema } from "@lpgroup/yup";

import { Organisations } from "./organisations.class";
import hooks from "./organisations.hooks";
import schema from "./organisations.yup";

export default (app, optionsOverride) => {
  const options = {
    id: "alias",
    paginate: app.get("paginate"),
    schema: mergeValidationsSchema(schema, optionsOverride.schema),
    whitelist: ["$regex"],
  };
  app.use("/organisations", new Organisations(options, app));
  const service = app.service("organisations");
  service.hooks(hooks);
};
