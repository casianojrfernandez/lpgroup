import * as yup from "@lpgroup/yup";

export const requestSchema = {
  _id: yup.id(),
  alias: yup.string().required(),
  name: yup.string().required(),
  organisationNumber: yup.string().defaultNull(),
  email: yup.string().email().defaultNull(),
  phone: yup.phone().defaultNull(),
  website: yup.string().url().defaultNull(),
  contact: yup.emailContact(),
  postAddress: yup.address(),
};

const dbSchema = {
  added: yup.changed(),
  changed: yup.changed(),
  owner: yup.owner(),
};

export default yup.buildValidationSchema(requestSchema, dbSchema);
