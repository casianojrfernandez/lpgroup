import { loadData, patchData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import { validateDatabase, validateRequest } from "@lpgroup/yup";
import search from "feathers-mongodb-fuzzy-search";

import auth from "../../hooks/auth";
import changed from "../../hooks/changed";
import grantPrivileges from "./hooks/grant-privileges";
import setOwner from "./hooks/set-owner-organisation";

export default {
  before: {
    all: [],
    find: [
      auth("public"),
      search({
        // regex search on given fields
        fields: ["alias", "name", "organisationNumber", "email", "phone", "website"],
      }),
    ],
    get: [auth("public")],
    create: [
      auth(),
      validateRequest(),
      changed(),
      setOwner(),
      validateDatabase(),
      grantPrivileges(),
    ],
    update: [auth(), validateRequest(), loadData(), changed(), validateDatabase()],
    patch: [auth(), validateRequest(), patchData(), changed(), validateDatabase()],
    remove: [auth()],
  },

  after: {
    all: [url({ key: "alias" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
