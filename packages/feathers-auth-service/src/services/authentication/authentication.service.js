import fAuthentication from "@feathersjs/authentication";
import fOauth from "@feathersjs/authentication-oauth";
import hooks from "./authentication.hooks";
import { LocalStrategy } from "./local-strategy";
import { DeviceStrategy } from "./device-strategy";

const { AuthenticationService, JWTStrategy } = fAuthentication;
const { expressOauth } = fOauth;

export default (app) => {
  const authentication = new AuthenticationService(app);

  authentication.register("jwt", new JWTStrategy());
  authentication.register("local", new LocalStrategy());
  authentication.register("device", new DeviceStrategy());

  app.use("/authentication", authentication);
  app.configure(expressOauth());

  // Get our initialized service so that we can register hooks
  const service = app.service("authentication");

  service.hooks(hooks);
};
