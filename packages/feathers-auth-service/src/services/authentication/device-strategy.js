/* eslint-disable class-methods-use-this */
import errors from "@feathersjs/errors";
import { internalParams } from "@lpgroup/feathers-utils";
import authentication from "@feathersjs/authentication";

export class DeviceStrategy extends authentication.AuthenticationBaseStrategy {
  get configuration() {
    const authConfig = this.authentication.configuration;
    const config = super.configuration || {};
    return {
      service: authConfig.service,
      entity: authConfig.entity,
      errorMessage: "Invalid login",
      ...config,
    };
  }

  async findEntity(deviceId, params) {
    const { entityService } = this;
    const { errorMessage } = this.configuration;
    try {
      const result = await entityService.get(
        deviceId,
        internalParams(params, { type: "device", active: true })
      );
      return result;
    } catch (err) {
      throw new errors.NotAuthenticated(errorMessage);
    }
  }

  async authenticate(data, params) {
    const { entity } = this.configuration;
    const deviceId = data._id;
    const result = await this.findEntity(deviceId, internalParams(params));
    return {
      authentication: { strategy: this.name },
      [entity]: result,
    };
  }
}
