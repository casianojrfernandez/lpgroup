import { disableSync } from "@lpgroup/feathers-utils/hooks";
import { validateRequest } from "@lpgroup/yup";
import feathersHooksCommon from "feathers-hooks-common";

import clearProvider from "../../hooks/clear-provider";
import schema from "./authentication.yup";

const { disallow, keep } = feathersHooksCommon;

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [clearProvider(), validateRequest({ schema })],
    update: [disallow()],
    patch: [disallow()],
    remove: [],
  },

  after: {
    all: [
      disableSync(),
      keep(
        "accessToken",
        "user._id",
        "user.type",
        "user.firstName",
        "user.lastName",
        "user.phone",
        "user.email",
        "user.privileges"
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
