import * as cy from "@lpgroup/yup";

const requestSchema = {
  strategy: cy
    .string()
    .matches(/(jwt|local|device)/)
    .lowercase()
    .default("local"),
  accessToken: cy.string(),
  _id: cy.uuid().defaultNull(),
  email: cy.email().defaultNull(),
  password: cy.string().defaultNull(),
};
export default cy.buildValidationSchema(requestSchema);
