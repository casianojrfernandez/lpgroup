import { getPermissions } from "../../hooks/check-permissions";

export class UsersPermissions {
  constructor(options, app) {
    this.app = app;
  }

  async find(params) {
    return getPermissions({ app: this.app, params });
  }
}
