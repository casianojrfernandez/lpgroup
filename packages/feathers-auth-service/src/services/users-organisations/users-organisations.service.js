import { UsersOrganisations } from "./users-organisations.class";
import hooks from "./users-organisations.hooks";

export default (app) => {
  const options = {
    id: "_id",
  };

  app.use("/users/:userId/organisations", new UsersOrganisations(options, app));
  const service = app.service("users/:userId/organisations");
  service.hooks(hooks);
};
