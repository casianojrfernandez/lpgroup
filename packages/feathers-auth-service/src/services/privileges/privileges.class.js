import { Service } from "feathers-mongodb";
import { onPluginReady } from "@lpgroup/feathers-plugins";

export class Privileges extends Service {
  constructor(options) {
    super(options);
    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("privileges");
      database.createIndexThrowError("privileges", { alias: 1 }, { unique: true });
    });
  }
}
