import feathersHooksCommon from "feathers-hooks-common";
import { validateRequest, validateDatabase } from "@lpgroup/yup";
import { patchData, loadData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import changed from "../../hooks/changed";
import auth from "../../hooks/auth";

const { iff, isProvider } = feathersHooksCommon;

export default {
  before: {
    all: [iff(isProvider("external"), auth())],
    find: [],
    get: [],
    create: [validateRequest(), changed(), validateDatabase()],
    update: [validateRequest(), loadData(), changed(), validateDatabase()],
    patch: [validateRequest(), patchData(), changed(), validateDatabase()],
    remove: [],
  },

  after: {
    all: [url({ key: "_id" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
