import { Privileges } from "./privileges.class";
import hooks from "./privileges.hooks";
import schema from "./privileges.yup";

export default (app) => {
  const options = {
    id: "alias",
    paginate: app.get("paginate"),
    schema,
  };
  app.use("/privileges", new Privileges(options, app));
  const service = app.service("privileges");
  service.hooks(hooks);
};
