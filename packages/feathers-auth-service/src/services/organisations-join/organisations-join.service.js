import { OrganisationsJoin } from "./organisations-join.class";
import hooks from "./organisations-join.hooks";

export default (app) => {
  const options = {
    id: "_id",
  };

  app.use("/organisations/:organisationAlias/join", new OrganisationsJoin(options, app));
  const service = app.service("organisations/:organisationAlias/join");
  service.hooks(hooks);
};
