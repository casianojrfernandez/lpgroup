import { disableSync } from "@lpgroup/feathers-utils/hooks";
import feathersHooksCommon from "feathers-hooks-common";

import auth from "../../hooks/auth";
import emitJoin from "./hooks/emit-join";

const { disallow } = feathersHooksCommon;

export default {
  before: {
    all: [auth()],
    find: [disallow()],
    get: [disallow()],
    create: [],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [disableSync()],
    find: [],
    get: [],
    create: [emitJoin()],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
