import { internalParams } from "@lpgroup/feathers-utils";
import { pick } from "lodash-es";

async function getOrganisation(alias, app, params) {
  const organisation = await app.service("organisations").get(alias, internalParams(params));
  return pick(organisation, ["_id", "alias", "name", "title"]);
}

export class OrganisationsJoin {
  constructor(options, app) {
    this.app = app;
  }

  async create(data, params) {
    const organistion = await getOrganisation(params.query.organisationAlias, this.app, params);
    this.app.channel(`organisations-${organistion.alias}`).join(params.connection);
    return organistion;
  }
}
