import { internalParams } from "@lpgroup/feathers-utils";
import authentication from "@feathersjs/authentication";
import feathersHooksCommon from "feathers-hooks-common";
// eslint-disable-next-line import/no-named-default
import { default as checkPermissions } from "./check-permissions";

const { authenticate } = authentication.hooks;
const { combine } = feathersHooksCommon;

async function setUser(context, defaultUserEmail) {
  const user = await context.app
    .service("users")
    .find(internalParams({ ...context.params, superUser: true }, { email: defaultUserEmail }));
  // eslint-disable-next-line prefer-destructuring
  context.params.user = user.data[0];
  return context;
}

export default (extraPrivilege, defaultUserEmail = null) => {
  return async (context) => {
    // Only validate auth token if it exist. Otherwise let checkPermission return
    // error message, if no public privilegies exist.
    if (context.params.authentication) {
      return combine(authenticate("jwt"), checkPermissions(extraPrivilege)).call(this, context);
    }

    if (defaultUserEmail) await setUser(context, defaultUserEmail);
    return checkPermissions(extraPrivilege)(context);
  };
};
