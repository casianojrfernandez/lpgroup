import debugRoot from "debug";
import { union } from "lodash-es";
import errors from "@feathersjs/errors";
import { internalParams } from "@lpgroup/feathers-utils";

const debug = debugRoot("auth");

/**
 * Get privileges from mongodb for loged in user
 * TODO: Optimize: This can probably be prepopulated on users, like Martin did before.
 *       Websocket will reuse the same context.params.user
 */
async function getPrivileges(context, userPrivileges) {
  const { params } = context;
  const aliases = userPrivileges.map((v) => v.privilegesAlias);
  const privileges = await context.app
    .service("privileges")
    .find(internalParams(params, { alias: { $in: aliases } }));
  return privileges;
}

/**
 * Extract permissions from privileges db result ordered by alias
 */
function extractPermissions(privileges) {
  /* eslint-disable no-param-reassign */
  return privileges.data.reduce((permissions, privilege) => {
    permissions[privilege.alias] = privilege.permissions;
    return permissions;
  }, {});
  /* eslint-enable no-param-reassign */
}

function filterPermissions(userPrivileges, permissions, service, method) {
  /* eslint-disable no-param-reassign */
  return userPrivileges.reduce((queries, userPrivilege) => {
    if (
      permissions[userPrivilege.privilegesAlias] &&
      permissions[userPrivilege.privilegesAlias][service]
    ) {
      const { query, methods } = permissions[userPrivilege.privilegesAlias][service];
      if (methods.includes("*") || methods.includes(method)) {
        query.forEach(({ key, param }) => {
          const value = param in userPrivilege.params ? userPrivilege.params[param] : param;
          if (value) {
            if (key in queries) queries[key].push(value);
            else queries[key] = [value];
          }
        });
      }
    }
    return queries;
  }, {});
  /* eslint-enable no-param-reassign */
}

function validatePermissions(context, permissions) {
  const { path, method } = context;
  // If params does't have any user (no one is logged in), set default value.
  const { email = "NA" } = context.params.user || {};
  if (Object.keys(permissions).length > 0) {
    debug(`Grants ${email} ${method} ${path} ${JSON.stringify(permissions)}`);
  } else {
    debug(`Access denied ${email} ${path} ${method} ${JSON.stringify(permissions)}`);
    throw new errors.MethodNotAllowed(`Access denied ${method} ${path} `, {
      path,
      method,
    });
  }
}

const onlyHasStarValues = ([, values]) => values.every((value) => value === "*");

/**
 * Remove query params that should return all rows ie *.
 */
function convertToFeathersJsQuery(permissions) {
  const permissionsArr = Object.entries(permissions);

  // If any permission group only has * persmissions. The specific
  // permissions in another group should be ignored.
  // ie.
  // super_user role has access to all through _id === "*"
  // standard_organisation has access to only cafe-minimal through alias === "cafe-minimal"
  if (permissionsArr.some(onlyHasStarValues)) return {};

  const query = permissionsArr.reduce((result, [key, values]) => {
    const ids = values.filter((v) => v !== "*");
    const value = ids.length > 1 ? { $in: ids } : ids[0];
    if (ids.length > 0) result.push({ [key]: value });
    return result;
  }, []);

  if (Object.keys(query).length > 1) return { $or: query };
  return query[0] || {};
}

// TODO: Rename this, this module is also called checkPermissions and this confuses stuff.
export function checkPermissions(context, userPrivileges, privileges) {
  const { path, method } = context;
  const rawPermissions = extractPermissions(privileges);
  const permissions = filterPermissions(userPrivileges, rawPermissions, path, method);
  validatePermissions(context, permissions);
  const query = convertToFeathersJsQuery(permissions);
  return query;
}

function addRestrictionQuery(context, query) {
  Object.assign(context.params.query, query);
}

export function buildUserPrivileges(context, extraUserPrivilege) {
  const { params } = context;
  const result = [];
  if (params.user && params.user.privileges) result.push(...params.user.privileges);
  if (extraUserPrivilege) result.push({ privilegesAlias: extraUserPrivilege, params: {} });
  return result;
}

function extractServicePermissions(privileges) {
  return privileges.data.reduce((servicePermissions, privilege) => {
    Object.entries(privilege.permissions).forEach(([service, permissions]) => {
      // eslint-disable-next-line no-param-reassign
      servicePermissions[service] = union(servicePermissions[service] || [], permissions.methods);
    });
    return servicePermissions;
  }, {});
}

export async function getPermissions(context, extraUserPrivilege = "") {
  const userPrivileges = buildUserPrivileges(context, extraUserPrivilege);
  const privileges = await getPrivileges(context, userPrivileges);
  const permissions = extractServicePermissions(privileges);
  return permissions;
}

export default (extraUserPrivilege = "") => {
  return async (context) => {
    if (!context.params.superUser) {
      const userPrivileges = buildUserPrivileges(context, extraUserPrivilege);
      const privileges = await getPrivileges(context, userPrivileges);
      const query = checkPermissions(context, userPrivileges, privileges);
      addRestrictionQuery(context, query);
    }
    return context;
  };
};
