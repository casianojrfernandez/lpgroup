#!/bin/sh
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


pushd .

echo "Change to lpgroup root folder"
echo "============================="
cd $SCRIPT_DIR
cd ..
pwd
echo

echo "Link all lpgroup packages into current node installation"
echo "========================================================"
cd packages/feathers-auth-service && yarn link && cd ../..
cd packages/feathers-k8s-probe-services && yarn link && cd ../..
cd packages/feathers-mongodb-hooks && yarn link && cd ../..
cd packages/feathers-plugins && yarn link && cd ../..
cd packages/feathers-utils && yarn link && cd ../..
cd packages/ghostscript && yarn link && cd ../..
cd packages/import-cli && yarn link && cd ../..
cd packages/utils && yarn link && cd ../..
cd packages/yup && yarn link && cd ../..
popd

echo
echo "Link current project to lpgroup"
echo "==============================="
yarn link @lpgroup/feathers-auth-service
yarn link @lpgroup/feathers-k8s-probe-services
yarn link @lpgroup/feathers-mongodb-hooks
yarn link @lpgroup/feathers-plugins
yarn link @lpgroup/feathers-utils
yarn link @lpgroup/ghostscript
yarn link @lpgroup/import-cli
yarn link @lpgroup/utils
yarn link @lpgroup/yup
