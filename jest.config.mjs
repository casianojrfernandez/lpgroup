export default {
  // Automatically clear mock calls and instances before every test.
  clearMocks: true,
  modulePathIgnorePatterns: ["feathers-auth-service-example"],
  roots: ["<rootDir>/packages"],
  setupFilesAfterEnv: ["<rootDir>/jest-setup-timeout.js"],
  testEnvironment: "node",

  testRegex: "(/test/.*.(test|spec)).(jsx?|tsx?|mjs|cjs)$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "mjs", "cjs", "json", "node"],

  collectCoverage: true,
  coveragePathIgnorePatterns: ["(test/.*.mock).(jsx?|tsx?|mjs|cjs)$"],
  coverageDirectory: "<rootDir>/coverage/",

  // Will print all tests
  verbose: true,
};
